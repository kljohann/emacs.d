;;; compile-in-dir.el --- Compile outside of default-dir

;; Copyright (C) 2014  Johann Klähn

;; Author: Johann Klähn <kljohann@gmail.com>
;; Keywords: tools, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'files)
(require 'compile)

(defvar compile-in-dir--last-dominating-file-name nil)

;;;###autoload
(defun compile-in-dir-with-dominating-file (name command &optional comint)
  ""
  (interactive
   (list (read-from-minibuffer "Dominating file: "
                               compile-in-dir--last-dominating-file-name)
         (let ((command (eval compile-command)))
           (if (or compilation-read-command current-prefix-arg)
               (compilation-read-command command)
             command))
         (consp current-prefix-arg)))
  (setq compile-in-dir--last-dominating-file-name name)
  (let* ((this-dir (or (ignore-errors (file-name-directory (buffer-file-name)))
                       default-directory))
         (default-directory
           (or (locate-dominating-file this-dir name) default-directory)))
    (when (yes-or-no-p (format "Compile in %s?" default-directory))
      (compile command comint))))

(provide 'compile-in-dir)
;;; compile-in-dir.el ends here

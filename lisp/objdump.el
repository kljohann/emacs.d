;;; objdump.el --- Inspect binaries -*- lexical-binding: t -*-

;; Copyright (C) 2017  Johann Klähn

;; Author: Johann Klähn <kljohann@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'rx)

(defvar objdump-command "objdump -d -M intel-mnemonic -Sl --no-show-raw-insn %s"
  "")

(defun objdump-refresh (&optional arg)
  ""
  (interactive "P")
  (when (>= (prefix-numeric-value arg) 16)
    (kill-local-variable 'objdump-command))
  (when arg
    (set (make-local-variable 'objdump-command)
         (read-string "objdump command: " objdump-command)))
  (objdump-mode 0)
  (objdump-mode 1))

(defun objdump--shadow-non-assembly-code ()
  ""
  (remove-overlays)
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-min))
      (while (progn
               (or (looking-at (rx (+ space) (+ hex-digit) ":" (+ space)))
                   (let ((eol (save-excursion (end-of-line) (point))))
                     (overlay-put (make-overlay (point) eol) 'face 'shadow)))
               (= (forward-line) 0))))))

;;;###autoload
(define-minor-mode objdump-mode
  ""
  :init-value nil
  :lighter " objdump"
  (let ((inhibit-read-only t))
    (remove-overlays)
    (erase-buffer)
    (if objdump-mode
        (progn
          (let ((command (format objdump-command (shell-quote-argument (buffer-file-name)))))
            (insert command)
            (insert (shell-command-to-string command)))
          (delete-trailing-whitespace)
          (nasm-mode)
          ;; somehow this is overwritten by nasm-mode
          (setq objdump-mode t)
          (objdump--shadow-non-assembly-code))
      (fundamental-mode)
      (insert-file-contents (buffer-file-name)))
    (set-buffer-modified-p nil)
    (read-only-mode 1)))

(provide 'objdump)
;;; objdump.el ends here

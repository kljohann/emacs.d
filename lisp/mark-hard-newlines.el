;;; mark-hard-newlines.el --- Make hard newlines visible.  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Johann Klähn

;; Author: Johann Klähn <johann@jklaehn.de>
;; Keywords: text, mail

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'cl-macs)
(require 'track-changes)

(defgroup mark-hard-newlines nil
  "Make hard newlines visible."
  :group 'faces)

(defcustom mark-hard-newlines-marker "⏎\n"
  "A string to display instead of hard newlines.
This is only used if `mark-hard-newlines-mode' is turned on."
  :type 'string
  :set (lambda (symbol value)
         (unless (string-suffix-p "\n" value)
           (setq value (concat value "\n")))
         (set-default symbol value)))

(defface mark-hard-newlines-marker '((t (:inherit shadow)))
  "Face for marker used to show hard newlines.
This face is only used if `mark-hard-newlines-mode' is turned on."
  :group 'mark-hard-newlines)

(defvar-local mark-hard-newlines--track-changes nil)

(defun mark-hard-newlines--track-changes-signal (tracker)
  (cl-assert (eq tracker mark-hard-newlines--track-changes))
  (track-changes-fetch tracker #'mark-hard-newlines--update-marks))

(defun mark-hard-newlines--update-marks (beg end &rest _)
  "Make hard newlines between BEG and END visible."
  (cl-loop for pos = beg
           then (and (< pos end)
                     (text-property-not-all (1+ pos) end 'hard nil)) while pos
           when (and (eq (char-after pos) ?\n)
                     (get-text-property pos 'hard))
           do (put-text-property
               pos (1+ pos) 'display
               (copy-sequence (propertize mark-hard-newlines-marker
                                          'face 'mark-hard-newlines))))
  (when mark-hard-newlines--track-changes
    ;; Ignore the changes we just made.
    (track-changes-fetch mark-hard-newlines--track-changes #'ignore)))

(define-minor-mode mark-hard-newlines-mode
  "Make hard newlines visible."
  :group 'paragraphs
  :lighter " ⏎"
  (if mark-hard-newlines-mode
      (progn
        (mark-hard-newlines--update-marks (point-min) (point-max))
        (setq mark-hard-newlines--track-changes
              (track-changes-register
               #'mark-hard-newlines--track-changes-signal :nobefore t)))
    (when mark-hard-newlines--track-changes
      (track-changes-unregister mark-hard-newlines--track-changes)
      (setq mark-hard-newlines--track-changes nil))
    (cl-loop for pos = (point-min)
             then (and (< pos (point-max))
                       (text-property-not-all (1+ pos) (point-max) 'hard nil)) while pos
             when (and (eq (char-after pos) ?\n)
                       (get-text-property pos 'hard))
             do (remove-text-properties pos (1+ pos) '(display nil)))))

(provide 'mark-hard-newlines)
;;; mark-hard-newlines.el ends here

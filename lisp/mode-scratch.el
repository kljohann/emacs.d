;;; mode-scratch.el --- mode-specific scratch buffers -*- lexical-binding: t; -*-

;; Copyright (C) 2015-2024  Johann Klähn

;; Author: Johann Klähn <kljohann@gmail.com>
;; URL: https://github.com/kljohann/mode-scratch.el
;; Version: 0.2.0
;; Package-Requires: ((emacs "26.1"))
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides a convenient way to create mode-specific scratch buffers.
;; It allows you to quickly create a scratch buffer in any major mode, optionally
;; inserting the active region's contents.

;;; Code:

(require 'subr-x)

(defgroup mode-scratch nil
  "Mode-specific scratch buffers."
  :group 'convenience
  :prefix "mode-scratch-")

(defcustom mode-scratch-mode-alist
  '((sql-interactive-mode . sql-mode)
    (shell-mode . sh-mode)
    (inferior-ess-mode
     . (lambda ()
         (pcase ess-dialect
           ("R" 'R-mode)
           ("Julia" 'julia-mode)
           (_ 's-mode))))
    (inferior-python-mode . python-mode))
  "Alist mapping major modes to their corresponding scratch buffer modes.
Each element is a cons cell (MAJOR-MODE . SCRATCH-MODE), where
SCRATCH-MODE can be a symbol or a function returning a symbol."
  :type '(alist :key-type symbol :value-type (choice symbol function))
  :group 'mode-scratch)

(defun mode-scratch--guess-major-mode ()
  "Guess the appropriate scratch buffer major mode based on the current buffer."
  (let ((mode (or (alist-get major-mode mode-scratch-mode-alist) major-mode)))
    (if (functionp mode)
        (funcall mode)
      mode)))

(defun mode-scratch--choose-major-mode (&optional initial)
  "Prompt user to choose a major mode for the scratch buffer.
INITIAL is the initially selected mode."
  (let ((mode-names
         (thread-last
           (append (mapcar #'cdr auto-mode-alist)
                   (flatten-tree major-mode-remap-defaults)
                   (flatten-tree major-mode-remap-alist))
           delete-dups
           (seq-filter #'symbolp)
           (seq-filter #'commandp)
           (mapcar #'symbol-name)))
        (default (if (symbolp initial)
                     (symbol-name initial)
                   initial)))
    (intern
     (completing-read
      "Scratch mode: " mode-names nil 'require-match nil nil default))))

;;;###autoload
(defun mode-scratch (&optional arg)
  "Create a new scratch buffer with a specific major mode.
With prefix ARG, create a new uniquely named scratch buffer."
  (interactive "P")
  (let* ((mode (mode-scratch--choose-major-mode
                (mode-scratch--guess-major-mode)))
         (contents (when (use-region-p)
                     (buffer-substring (region-beginning)
                                       (region-end))))
         (name (format "*%s-scratch*"
                       (string-remove-suffix "-mode" (symbol-name mode))))
         (name (if arg (generate-new-buffer-name name) name))
         (buf (get-buffer-create name)))
    (with-current-buffer buf
      (funcall mode)
      (when contents
        (save-excursion
          (goto-char (point-max))
          (unless (bolp)
            (insert "\n"))
          (insert contents))))
    (pop-to-buffer buf)))

(provide 'mode-scratch)
;;; mode-scratch.el ends here

;;; recipe-grid.el --- Org babel support for recipe grids  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Johann Klähn

;; Author: Johann Klähn <johann@jklaehn.de>
;; Keywords: languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'ob)

(defgroup recipe-grid nil
  "Recipe-grid support for Emacs."
  :group 'languages)

(defcustom recipe-grid-executable
  (or (executable-find "recipe-grid")
      "recipe-grid")
  "Location of the recipe-grid executable."
  :group 'recipe-grid
  :type '(file :must-match t)
  :risky t)

(defcustom recipe-grid-shot-scraper-executable
  (or (executable-find "shot-scraper")
      "shot-scraper")
  "Location of the shot-scraper executable."
  :group 'recipe-grid
  :type '(file :must-match t)
  :risky t)

(defvar org-babel-default-header-args:recipe-grid
  '((:results . "graphics file") (:exports . "results"))
  "Default arguments to use when evaluating a recipe-grid source block.")

(defun org-babel-execute:recipe-grid (body params)
  "Execute a block of recipe-grid code with org-babel.
This function is called by `org-babel-execute-src-block', which passes
the block contents as BODY and its header argumenst as PARAMS."
  (unless (assq :file params)
    (user-error "Missing :file header argument for recipe-grid block"))
  (let ((in-file (org-babel-temp-file "recipe-grid-" ".md")))
    (with-temp-file in-file
      (insert body)
      (indent-region (point-min) (point-max) 4))
    (org-babel-eval
     (concat (org-babel-process-file-name recipe-grid-executable)
             " "
             (org-babel-process-file-name in-file))
     "")
    (org-babel-eval
     (concat (org-babel-process-file-name recipe-grid-shot-scraper-executable)
             " shot --silent --padding 10 --omit-background --selector-all=.rg-recipe-block "
             (org-babel-process-file-name (file-name-with-extension in-file ".html"))
             " --output "
             (org-babel-process-file-name (org-babel-graphical-output-file params)))
     ""))
  nil)

(defun org-babel-prep-session:recipe-grid (_session _params)
  "Return an error because recipe-grid does not support sessions."
  (user-error "Sessions are not supported by recipe-grid"))

(provide 'recipe-grid)
;;; recipe-grid.el ends here

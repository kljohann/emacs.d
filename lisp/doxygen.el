;;; doxygen.el --- minor-mode for doxygen comments   -*- lexical-binding: t; -*-

;; Copyright (C) 2001-2010 Ryan T. Sammartino
;; Copyright (C) 2016  Johann Klähn

;; Author: Ryan T. Sammartino <ryan.sammartino at gmail dot com>
;;         Kris Verbeeck <kris.verbeeck at advalvas dot be>
;;         Johann Klähn <kljohann@gmail.com>
;; Keywords: c, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides font locking and adaptive filling for doxygen comments.

;;; Code:

(defgroup doxygen nil
  "Customization group for doxygen."
  :prefix "doxygen-"
  :group 'tools)

(defcustom doxygen-adaptive-fill nil
  "How paragraphs should be filled."
  :type '(choice
          (const :tag "After doxygen keyword" 'after-keyword)
          (const :tag "Disabled" nil))
  :group 'doxygen)

(defconst doxygen-adaptive-fill-regexp
  (rx (seq (any "@\\")
           ;; Note that entries that are a prefix of another entry need to
           ;; follow that entry.
           (or (seq "param" (? (* space) "[" (or "in" "out" "in,out") "]"))
               "arg" "attention" "authors" "author" "brief" "bug"
               "copyright" "date" "deprecated" "details" "exception"
               "invariant" "li" "note" "par" "post" "pre" "remarks"
               "remark" "result" "returns" "return" "retval" "sa" "see"
               "short" "since" "test" "throws" "throw" "todo" "tparam"
               "version" "warning" "xrefitem")
           (* blank))))

(defconst doxygen-keywords
  (list

   ;; \command without arguments
   (list
    (rx (group
         (any "@\\")
         (or (seq (? "hide") (or "call" "caller") "graph")
             (seq (or "show" "hide") (or "initializer" "refby" "refs"))
             (seq (? "end")
                  (or "code" "dot" "internal" "parblock"
                      "secreflist" "uml" "verbatim"))
             (seq (? "end")
                  (or "docbook" "html" "if" "latex" "man" "rtf" "xml")
                  "only")
             (seq (or "public" "protected" "private") (? "section"))
             "else" "endif" "fn" "li" "nosubgrouping" "pure" "static"

             ;; TODO: \n, \f$, \f[, \f], \f{env}{, \f}

             ;; TODO: The following have an until-eol argument:
             "overload" "mainpage" "name" "property" "typedef" "var"
             "vhdlflow" "addindex" "line" "skip" "skipline" "until"
             (seq (? "end") "cond")

             ;; TODO: The following have one paragraph as their argument:
             "arg" "author" "authors" "brief" "copyright" "date"
             "details" "invariant" "note" "par" "post" "pre" "remark"
             "remarks" "result" "return" "returns" "sa" "see" "short"
             "since" "test" "version")) word-end)
    '(0 font-lock-keyword-face prepend))

   ;; \command (warning face)
   (list
    (rx (group
         (any "@\\")
         (or "attention" "deprecated" "warning" "todo" "bug")) word-end)
    '(0 font-lock-warning-face prepend))

   ;; group open/close markers
   (list
    (rx (group (or "@{" "@}")) symbol-end)
    '(0 font-lock-keyword-face prepend))

   ;; \command <preproc-name>
   (list
    (rx (group (any "@\\") (or "def"))
        (+ space) (group (+ word)))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-preprocessor-face prepend))

   ;; \command <variable-name>
   (list
    (rx (group (any "@\\")
               (or (seq "param" (? (* space) "[" (or "in" "out" "in,out") "]"))
                   "a" "p"))
        (+ space) (group (+ word)))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-variable-name-face prepend))

   ;; \command <type-name>
   (list
    (rx (group (any "@\\")
               (or "enum" "exception" "extends" "idlexcept" "implements"
                   "memberof" "namespace" "package" "throw" "throws" "tparam"
                   (seq (or "relates" "related") (? "also"))

                   ;; TODO: These take further optional arguments:
                   ;; [<header-file>] [<header-name>]
                   "category" "class" "interface" "protocol"
                   "struct" "union"))
        (+ space)
        (group (+ (any word ":"))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-type-face prepend))

   ;; \command <value>
   (list
    (rx (group (any "@\\") "retval")
        (+ space)
        (group (+ (not (any "\n" space)))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-constant-face prepend))

   ;; \b <bold>
   (list
    (rx (group (any "@\\") "b")
        (+ space)
        (group (+ (not (any "\n" space)))))
    '(1 font-lock-keyword-face prepend)
    '(2 'bold prepend))

   ;; \c <monospace>
   (list
    (rx (group (any "@\\") "c")
        (+ space)
        (group (+ (not (any "\n" space)))))
    '(1 font-lock-keyword-face prepend)
    '(2 'underline prepend))

   ;; \e <italic>
   (list
    (rx (group (any "@\\") "e" (? "m"))
        (+ space)
        (group (+ (not (any "\n" space)))))
    '(1 font-lock-keyword-face prepend)
    '(2 'italic prepend))

   ;; \command <word>+
   (list
    (rx (group (any "@\\") "ingroup")
        (group (+ (+ space) (+ word))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-builtin-face prepend))

   ;; \command <no-space>
   (list
    (rx (group (any "@\\")
               (or (seq "copy" (or "doc" "brief" "details"))
                   (seq "if" (? "not")) "elseif" "link" "xrefitem"))
        (+ space)
        (group (+ (not (any "\n" space)))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-string-face prepend))

   ;; \command [<no-space>]
   (list
    (rx (group (any "@\\") (or "dir"))
        (? (+ space) (group (+ (not (any "\n" space))))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-string-face prepend t))

   ;; \~[<language>]
   (list
    (rx (group (any "@\\") "~")
        (opt (group (+ (not (any "\n" space))))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-builtin-face prepend t))

   ;; \command <file>
   (list
    (rx (group (any "@\\")
               (or (seq "example" (? "{lineno}"))
                   (seq (or "include" "snippet")
                        (? (or (seq "{" (or "lineno" "doc") "}")
                               (or "lineno" "doc"))))
                   (seq (or "dont" "html" "verb" "latex") "include")
                   "file" "headerfile"))
        (+ space)
        (group (? ?\") (+ (any "~:\\/a-zA-Z0-9_. ")) (? ?\")))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-preprocessor-face prepend))

   ;; \command <file> ["caption"]
   (list
    (rx (group (any "@\\") (or "dot" "dia" "msc") "file")
        (+ space)
        (group (? ?\") (+ (any "~:\\/a-zA-Z0-9_. ")) (? ?\"))
        (? (+ space) (group ?\" (+ (not (any "\""))) ?\")))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-preprocessor-face prepend)
    '(3 font-lock-string-face prepend t))

   ;; \command ["caption"] [<sizeindication>=<size>]
   (list
    (rx (group (any "@\\")
               (or "dot" "msc"
                   (seq "startuml" (? (* space) "{" (+ (not (any ?}))) "}"))))
        (? (+ space) (group ?\" (+ (not (any "\""))) ?\"))
        (? (+ space) (group (+ word) ?= (+ digit) (+ word))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-string-face prepend t)
    '(3 font-lock-constant-face prepend t))

   ;; \image <format> <file> ["caption"] [<sizeindication>=<size>]
   (list
    (rx (group (any "@\\") "image")
        (+ space)
        (group (or "html" "latex"))
        (+ space)
        (group (? ?\") (+ (any "~:\\/a-zA-Z0-9_. ")) (? ?\"))
        (? (+ space) (group ?\" (+ (not (any "\""))) ?\"))
        (? (+ space) (group (+ word) ?= (+ digit) (+ word))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-builtin-face prepend)
    '(3 font-lock-preprocessor-face prepend)
    '(4 font-lock-string-face prepend t)
    '(5 font-lock-constant-face prepend t))

   (list
    (rx (group (any "@\\")
               (or "addtogroup" "defgroup" "weakgroup"))
        (+ space) (group (+ word))
        (? (+ space) (group (+ nonl)) eol))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-builtin-face prepend)
    '(3 font-lock-string-face prepend t))

   (list
    (rx (group (any "@\\") "emoji")
        (+ space)
        (group (or (+ word) (seq ":" (+ word) ":"))))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-string-face prepend))

   ;; \command <word>
   (list
    (rx (group (any "@\\")
               (or (seq (* "sub") "page")
                   (seq (* "sub") "section")
                   "anchor" "cite" "paragraph" "ref" "refitem"))
        (+ space) (group (+ word)))
    '(1 font-lock-keyword-face prepend)
    '(2 font-lock-builtin-face prepend))))

(defun doxygen-font-lock-on ()
  "Turn on font-lock for Doxygen keywords."
  (interactive)
  (font-lock-add-keywords nil doxygen-keywords))

(defun doxygen-font-lock-off ()
  "Turn off font-lock for Doxygen keywords."
  (interactive)
  (font-lock-remove-keywords nil doxygen-keywords))

(defun doxygen-adaptive-fill-function ()
  "Return prefix for filling paragraph or nil if not determined."
  (let ((comment-line-prefix
         (rx-to-string
          `(seq (* blank)
                (regexp ,(or (when (boundp 'c-current-comment-prefix)
                               c-current-comment-prefix)
                             ""))
                (* blank))
          'no-group)))
    (cond ((eql doxygen-adaptive-fill 'after-keyword)
           (when (looking-at (rx-to-string `(seq (group (regexp ,comment-line-prefix))
                                                 (group (regexp ,doxygen-adaptive-fill-regexp)))))
             (concat (match-string-no-properties 1)
                     (replace-regexp-in-string "." " " (match-string-no-properties 2))))))))

(defun doxygen-fill-nobreak-p ()
  "Don't break a line following inline markup directives."
  (save-excursion
    (skip-chars-backward " \t")
    (and (/= (skip-syntax-backward "w") 0)
         (memq (preceding-char) '(?@ ?\\)))))

;;;###autoload
(define-minor-mode doxygen-mode
  "Minor mode for Doxygen documentation."
  :lighter " doxy"

  (if doxygen-mode
      (progn
        (doxygen-font-lock-on)
        (add-hook 'fill-nobreak-predicate #'doxygen-fill-nobreak-p nil 'local)
        (setq-local
         c-paragraph-start
         (rx-to-string `(or eol (regexp ,doxygen-adaptive-fill-regexp))))
        (setq-local adaptive-fill-function #'doxygen-adaptive-fill-function))
    (doxygen-font-lock-off)
    (remove-hook 'fill-nobreak-predicate #'doxygen-fill-nobreak-p 'local)
    ;; Unfortunately we cannot just kill `c-paragraph-start', since it is only
    ;; ever set locally.  Instead we assign its normal value.
    ;; FIXME: What is the correct value here?
    (setq-local c-paragraph-start (rx eol))
    ;; (kill-local-variable 'c-paragraph-start)
    (kill-local-variable 'adaptive-fill-function))

  ;; Recompute paragraph variables
  (when (bound-and-true-p c-buffer-is-cc-mode)
    (c-setup-paragraph-variables))

  ;; Refresh font-locking when used interactively
  (font-lock-mode 1))

(provide 'doxygen)
;;; doxygen.el ends here

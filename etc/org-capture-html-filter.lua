function Header(elem)
    return pandoc.Strong(elem.content)
end

-- Remove anchor tags that are only used for internal page navigation
function Link(elem)
    if #elem.content == 0
        and (elem.target == "" or elem.classes:includes("anchor"))
    then
        return {}
    end
    return elem
end

-- Avoid property drawers and <<anchors>>
function Div(elem)
    elem.identifier = ""
    elem.attributes = {}
    return elem
end

function LineBreak()
    return {}
end

;;; init.el ---                                      -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(defvar epkg-repository)
(setq epkg-repository (locate-user-emacs-file "var/epkgs/"))
(add-to-list 'load-path (locate-user-emacs-file "lisp"))

;;; init.el ends here

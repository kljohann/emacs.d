;;; early-init.el --- early user init file           -*- lexical-binding: t; -*-

(setq load-prefer-newer t)

(when (fboundp 'startup-redirect-eln-cache)
  (startup-redirect-eln-cache "var/eln-cache/"))

(let ((dir (file-name-directory (or load-file-name buffer-file-name))))
  (add-to-list 'load-path (expand-file-name "lib/compat" dir))
  (add-to-list 'load-path (expand-file-name "lib/auto-compile" dir)))
(require 'auto-compile)
(auto-compile-on-load-mode)
(auto-compile-on-save-mode)

(setq package-enable-at-startup nil)

(defun disable-gc-and-file-handlers-until-idle+ (secs)
  "Disable garbage collection and file name handlers until Emacs is idle for SECS seconds."
  (interactive "NSeconds: ")
  (let ((prev-handlers file-name-handler-alist))
    (message "Disabling GC and file name handlers for %s second(s)." secs)
    (setq file-name-handler-alist nil)
    (setq gc-cons-percentage 1.0)
    (run-with-idle-timer
     secs nil
     (lambda ()
       (setq gc-cons-percentage (car (get 'gc-cons-percentage 'standard-value)))
       (setq file-name-handler-alist (append prev-handlers file-name-handler-alist))
       (delete-dups file-name-handler-alist)
       (message "Restored `gc-cons-percentage' and `file-name-handler-alist'.")))))
(disable-gc-and-file-handlers-until-idle+ 0.5)

;; Local Variables:
;; no-byte-compile: t
;; End:
;;; early-init.el ends here

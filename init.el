;;; init.el --- user init file                       -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(progn
  (defvar before-user-init-time (current-time)
    "Value of `current-time' when Emacs begins loading `user-init-file'.")
  (message "Loading Emacs...done (%.3fs)"
           (float-time (time-subtract before-user-init-time
                                      before-init-time)))
  ;; Allow loading configuration via `emacs -Q --load path/to/init.el'.
  (setq user-init-file (or load-file-name buffer-file-name))
  (setq user-emacs-directory (file-name-directory user-init-file))
  (message "Loading %s..." user-init-file)

  (setq inhibit-startup-screen t
        inhibit-startup-buffer-menu t
        initial-buffer-choice t
        initial-scratch-message
        (concat
         ";;                 -*- lexical-binding: t -*-\n"
         ";;\n"
         ";;  [00]   - I've never heard anything as ridiculous\n"
         ";; /|__|\\    as the idea of having a Lisp Machine.\n"
         ";;   ''\n"))

  ;; Disable menu bar in terminal.
  (setq window-system-default-frame-alist '((nil . ((menu-bar-lines . 0)))))

  (setq default-frame-alist
        '((fullscreen . maximized)
          (tool-bar-lines . 0)
          (vertical-scroll-bars . nil)
          (horizontal-scroll-bars . nil)))
  (menu-bar-mode 1)
  (tooltip-mode 0)
  (blink-cursor-mode 0))

(eval-and-compile
  (add-to-list 'load-path (locate-user-emacs-file "lisp")))

(eval-and-compile
  (add-to-list 'load-path (locate-user-emacs-file "lib/borg"))
  (require 'borg)
  (borg-initialize))

(eval-and-compile
  (setq use-package-compute-statistics nil
        use-package-expand-minimally t)
  (setq use-package-compute-statistics t
        use-package-expand-minimally nil
        use-package-verbose t
        use-package-enable-imenu-support t
        use-package-ensure-function #'ignore)
  (require 'use-package))

(require 'dash)
(require 'f)
(require 'json)
(require 's)
(require 'subr-x)
(require 'treesit)
(require 'url-parse)

(defsubst my:frame-small-p ()
  "Return t if frame is \"small\"."
  (< (frame-height) 40))

(defsubst my:frame-wide-p ()
  "Return t if frame is \"wide\"."
  (> (frame-width) 200))

(defsubst my:frame-portrait-p ()
  "Return t if frame is higher than wide."
  (< (frame-width) (frame-height)))

(defsubst my:frame-landscape-p ()
  "Return t if frame is wider than high."
  (not (my:frame-portrait-p)))

(use-package auto-compile
  :no-require t

  :custom
  (auto-compile-display-buffer nil)
  (auto-compile-mode-line-counter t)
  (auto-compile-check-parens t)
  (auto-compile-source-recreate-deletes-dest t)
  (auto-compile-toggle-deletes-nonlib-dest t)
  (auto-compile-update-autoloads t)
  (auto-compile-on-load-mode t)
  (auto-compile-on-save-mode nil)

  :config
  (add-hook 'auto-compile-inhibit-compile-hook
            'auto-compile-inhibit-compile-detached-git-head))

(use-package diminish
  :demand t)

(use-package no-littering
  :demand t

  :functions no-littering-theme-backups
  :config
  (no-littering-theme-backups))

(use-package custom
  :no-require t

  :config
  (setq custom-file (no-littering-expand-etc-file-name "custom.el")
        custom-theme-directory (no-littering-expand-etc-file-name "themes"))
  (when (file-exists-p custom-file)
    (with-demoted-errors "Could not load `custom-file': %S" (load-file custom-file))))

(defun hooks:remember-current-theme ()
  "Remember last theme on exit."
  (customize-save-variable 'custom-enabled-themes custom-enabled-themes))
(add-hook 'kill-emacs #'hooks:remember-current-theme)

(set-face-background 'glyphless-char "red")

(use-package epkg
  :defer t

  :custom
  (epkg-database-connector 'sqlite-builtin))

(use-package emacs
  :custom
  (truncate-partial-width-windows 50)
  (truncate-lines nil)
  (fill-column 80)

  (scroll-margin 3)
  ;; TODO: reconsider?
  (scroll-conservatively 99)
  (next-screen-context-lines 3)
  (line-spacing 0.05)

  (indicate-buffer-boundaries t)

  (echo-keystrokes 0.01)
  (frame-inhibit-implied-resize t)
  (frame-title-format '(buffer-file-name "%f" ("%b")))
  (visible-bell t)

  (message-log-max (expt 2 14))
  (history-length 500)
  (history-delete-duplicates t)

  (delete-by-moving-to-trash t)
  (create-lockfiles nil)
  (remote-file-name-inhibit-locks t)

  (x-selection-timeout 200) ;; in ms
  (read-process-output-max (* 1024 1024))

  (widget-image-enable nil)
  (use-short-answers nil)
  (use-dialog-box nil)

  :config
  ;; (put 'file-name-history 'history-length 500)
  (prefer-coding-system 'utf-8-unix))

(defun hooks:emacs/minibuffer-setup ()
  "Disable garbage cllection upon entering minibuffer."
  (setq gc-cons-percentage 1.0))
(add-hook 'minibuffer-setup-hook #'hooks:emacs/minibuffer-setup)

(defun hooks:emacs/minibuffer-exit ()
  "Reset garbage collection threshold upon leaving minibuffer."
  (setq gc-cons-percentage (car (get 'gc-cons-percentage 'standard-value))))
(add-hook 'minibuffer-exit-hook #'hooks:emacs/minibuffer-exit)

(use-package evil
  :demand t

  :init
  (setq evil-respect-visual-line-mode t)

  :custom
  (evil-echo-state nil)
  (evil-move-beyond-eol t)
  (evil-move-cursor-back nil)
  (evil-highlight-closing-paren-at-point-states nil)
  (evil-intercept-esc t)
  (evil-search-module 'isearch)
  (evil-symbol-word-search t)
  (evil-want-C-d-scroll nil)
  (evil-want-C-i-jump nil)
  (evil-want-C-w-in-emacs-state t)
  (evil-want-Y-yank-to-eol t)
  (evil-overriding-maps
   '((Buffer-menu-mode-map . nil)
     (comint-mode-map . nil)
     (compilation-mode-map . nil)
     (dictionary-mode-map . nil)
     (grep-mode-map . nil)
     (Info-mode-map . motion)))

  :functions
  evil-define-key
  evil-define-key*
  evil-define-minor-mode-key
  evil-emacs-state
  evil-first-non-blank-of-visual-line
  evil-normal-state
  evil-set-command-property
  evil-set-initial-state

  :config
  (define-key evil-motion-state-map " " nil)

  ;; In some terminals emacs can't distinguish between TAB and C-i:
  (define-key evil-motion-state-map (kbd "C-i") nil)
  (define-key evil-motion-state-map (kbd "C-o") nil)

  (define-key evil-window-map (kbd "<left>") 'evil-window-left)
  (define-key evil-window-map (kbd "<down>") 'evil-window-down)
  (define-key evil-window-map (kbd "<up>") 'evil-window-up)
  (define-key evil-window-map (kbd "<right>") 'evil-window-right)

  (when evil-respect-visual-line-mode
    (evil-define-minor-mode-key 'motion 'visual-line-mode
      [remap evil-first-non-blank] #'evil-first-non-blank-of-visual-line
      [up] 'evil-previous-visual-line
      (kbd "g <up>") 'evil-previous-line
      [down] 'evil-next-visual-line
      (kbd "g <down>") 'evil-next-line))

  (evil-set-initial-state 'special-mode 'emacs)
  (evil-mode 1))

(defun evil-ex-search--center (&rest _args)
  "Recenter to current line."
  (evil-scroll-line-to-center (line-number-at-pos)))
(dolist (fun '(evil-ex-search-next
               evil-ex-search-previous
               evil-jump-forward
               evil-jump-backward))
  (advice-add fun :after #'evil-ex-search--center))

(defun evil-lookup+ (&optional prefix)
  "Look up documentation for the symbol at point."
  (interactive "P")
  (cond
   ((and (bound-and-true-p eglot--managed-mode)
         (bound-and-true-p eldoc-mode))
    (eldoc-doc-buffer t))
   ((derived-mode-p 'emacs-lisp-mode)
    (describe-symbol (symbol-at-point)))
   ((fboundp 'devdocs-lookup)
    (devdocs-lookup prefix (thing-at-point 'symbol t)))
   (t (user-error "No help for symbol at point."))))
(setq evil-lookup-func 'evil-lookup+)

(use-package general
  :demand t

  :config
  (add-to-list 'general-non-normal-states 'motion 'append)
  (eval-and-compile
    (general-evil-setup)
    (general-create-definer my:general/leader
      :states '(normal visual insert emacs motion)
      :non-normal-prefix "C-c SPC"
      :prefix "SPC"))
  (general-nvmap "'" (general-simulate-key "C-c")))

(define-key minibuffer-local-map (kbd "C-w") 'backward-kill-word)

(evil-define-key 'normal messages-buffer-mode-map "q" 'bury-buffer)
(general-nmap "g]" 'evil-jump-to-tag)

(general-mmap
  "] b" 'evil-next-buffer
  "[ b" 'evil-prev-buffer
  "] q" 'next-error
  "[ q" 'previous-error)

(general-nmap
  "C-q" 'universal-argument
  "C-r" nil
  "u" 'evil-undo
  "U" 'evil-redo)
(general-imap "C-v" 'quoted-insert)

(my:general/leader
  "q" 'save-buffers-kill-terminal
  "Q" 'save-buffers-kill-emacs

  "SPC" 'execute-extended-command

  "e" 'ff-find-other-file

  "w" 'evil-window-map

  "ESC" 'keyboard-escape-quit
  "0" 'text-scale-adjust
  "x" 'variable-pitch-mode

  "i" 'evil-jump-forward
  "o" 'evil-jump-backward)



(defun my:fonts/first-available (&rest fonts)
  "Return the first entry in FONTS that is available on this system."
  (catch 'found
    (dolist (props fonts)
      (let ((spec (apply 'font-spec :weight 'normal :slant 'normal props)))
        (when (find-font spec)
          (throw 'found props))))))

(defun reset-default-fonts+ (&optional point-size)
  "Set the fonts of the `default' and `variable-pitch' faces."
  (interactive "nFont size in point: ")
  (let* ((height (* 10 (max 6 (or point-size 10))))
         (font (my:fonts/first-available
               `(:family "PragmataPro" :height ,height)
               `(:family "PragmataPro Mono" :height ,height)
               `(:family "Fira Code" :height ,height)
               `(:family "Fira Mono" :height ,height)
               `(:family "Monospace" :height ,height))))
  (with-demoted-errors "Could not set default font: %S"
    (apply #'set-face-attribute 'default nil font))
  (with-demoted-errors "Could not set fixed-pitch font: %S"
    (apply #'set-face-attribute 'fixed-pitch nil :height 1.0 font)))
  (with-demoted-errors "Could not set variable-pitch font: %S"
    (apply #'set-face-attribute 'variable-pitch nil
           (my:fonts/first-available
            '(:family "Lato" :height 1.1)
            '(:family "IBM Plex Sans")
            '(:family "Sans Serif")))))

(general-after-gui
  (reset-default-fonts+)
  (set-fontset-font t 'symbol "Noto Color Emoji" nil 'append)
  (set-fontset-font t 'symbol "Symbola" nil 'append))

;; https://github.com/hlissner/.emacs.d/blob/master/core/core-modeline.el
(defun my:mode-line/selected-region ()
  (let ((reg-beg (region-beginning))
        (reg-end (region-end))
        (evil (evil-visual-state-p)))
    (let ((lines (count-lines reg-beg (min (1+ reg-end) (point-max))))
          (chars (- (1+ reg-end) reg-beg))
          (cols (1+ (abs (- (evil-column reg-end)
                            (evil-column reg-beg))))))
      (cond
       ;; rectangle selection
       ((or (bound-and-true-p rectangle-mark-mode)
            (and evil (eql 'block evil-visual-selection)))
        (format " %d×%d" lines (if evil cols (1- cols))))
       ;; line selection
       ((or (> lines 1) (eql 'line evil-visual-selection))
        (if (and (eql evil-state 'visual) (eql evil-this-type 'line))
            (format " %dL" lines)
          (format " %dC %dL" chars lines)))
       (t (format " %dC" (if evil chars (1- chars))))))))

(when (< emacs-major-version 30)
  (defvar mode-line-format-right-align nil))

(setq-default mode-line-format
              '("%e" mode-line-front-space
                " "
                (:eval
                 `(:propertize
                   (" "
                    (:eval (my:mode-line/pretty-number (window-parameter (selected-window) 'ace-window-path)))
                    mode-line-client
                    evil-mode-line-tag
                    " ")
                   face ,(my:mode-line/evil-mode-face)))
                mode-line-modified
                " "
                mode-line-buffer-identification
                mode-line-remote
                " " mode-line-position
                (:eval (when (or mark-active
                                 (evil-visual-state-p)
                                 (bound-and-true-p rectangle-mark-mode))
                         (my:mode-line/selected-region)))
                " "
                mode-line-misc-info
                '(:eval
                  (unless (and (eql buffer-file-coding-system (default-value 'buffer-file-coding-system))
                               (not current-input-method))
                    mode-line-mule-info))
                mode-line-format-right-align
                mode-line-modes
                mode-line-end-spaces)
              mode-line-buffer-identification
              (propertized-buffer-identification "%b")
              mode-line-client
              '(:eval (if (frame-parameter nil 'client) "×" " "))
              mode-line-position
              '((size-indication-mode (" " (-4 "%I")))
                (-3 "%p")
                (line-number-mode
                 (" %l" (column-number-mode ":%c"))))
              mode-line-remote
              '(:eval
                (let ((host (file-remote-p default-directory 'host)))
                  (when host
                    (propertize (concat "@" host) 'face
                                '(italic warning)))))
              mode-line-modified
              '(:eval
                (cond (buffer-read-only #(" × " 0 3 (face warning)))
                      ((buffer-modified-p) #(" ‼ " 0 3 (face error)))
                      (t #(" ✔ " 0 3 (face success))))))

(setq evil-mode-line-format nil
      evil-normal-state-tag "N"
      evil-insert-state-tag "I"
      evil-visual-state-tag "V"
      evil-operator-state-tag "O"
      evil-replace-state-tag "R"
      evil-motion-state-tag "M"
      evil-emacs-state-tag "E")

(defun my:mode-line/evil-mode-face ()
  ;; Don't propertize if we're not in the selected buffer
  (cond ((not (mode-line-window-selected-p)) nil)
        ((evil-insert-state-p)
         '(:background "#E8E2B7" :foreground "#323638"))
        ((evil-emacs-state-p)
         '(:background "#A082BD" :foreground "#E0E2E4"))
        ((evil-motion-state-p)
         '(:background "#1E90FF" :foreground "#E0E2E4"))
        ((evil-visual-state-p)
         '(:background "#308291" :foreground "#E0E2E4"))
        ((evil-operator-state-p)
         '(:background "#93C79F" :foreground "#323638"))))

(defun my:mode-line/update-window-hint ()
  "Update ace-window-path window parameter for all windows."
  (when (featurep 'ace-window)
    (avy-traverse
     (avy-tree (aw-window-list) aw-keys)
     (lambda (path leaf)
       (set-window-parameter
        leaf 'ace-window-path (apply #'string (reverse path)))))))

(add-hook 'window-configuration-change-hook #'my:mode-line/update-window-hint)

(defun my:mode-line/pretty-number (num)
  ""
  (unless (stringp num)
    (setq num (ignore-errors (number-to-string num))))
  (or (cond ((not (fboundp 'display-graphic-p)) nil)
            ((not (display-graphic-p)) nil)
            ((equal num "1")  "➊")
            ((equal num "2")  "➋")
            ((equal num "3")  "➌")
            ((equal num "4")  "➍")
            ((equal num "5")  "➎")
            ((equal num "6")  "❻")
            ((equal num "7")  "➐")
            ((equal num "8")  "➑")
            ((equal num "9")  "➒")
            ((equal num "0")  "➓"))
      (concat "(" num ")")))



(use-package ace-window
  :defer 60

  :autoload aw-select
  :commands ace-window-select+
  :general
  (my:general/leader "TAB" 'ace-window-select+)

  :custom
  (aw-scope 'frame)
  (aw-dispatch-always t)

  :defines aw-keys
  :config
  (defun ace-window-select+ (&optional count)
    "Wrapper for switch-window that accepts a target window as a prefix."
    (interactive "P")
    (if (not count)
        (ace-select-window)
      (let ((ref (number-to-string count)))
        (avy-traverse
         (avy-tree (aw-window-list) aw-keys)
         (lambda (path leaf)
           (when (string-equal ref (apply #'string (reverse path)))
             (aw-switch-to-window leaf))))))))

(use-package arc-mode
  :no-require t

  :mode ("\\.whl\\'" . archive-mode))

(use-package autorevert
  :no-require t

  :custom
  (global-auto-revert-mode t))

(use-package avy
  :defer t

  :general
  (my:general/leader
    "n" 'avy-goto-char-timer
    "t" 'avy-goto-word-0
    "g g" 'avy-goto-line
    "," 'avy-prev
    "." 'avy-next
    ";" 'avy-next
    "N" 'avy-pop-mark)

  :custom
  (avy-single-candidate-jump nil)
  (avy-keys '(?u ?i ?a ?e ?n ?r ?t ?d))
  (avy-dispatch-alist
   '((?k . avy-action-kill-move)
     (?K . avy-action-kill-stay)
     (?x . avy-action-teleport)
     (?m . avy-action-mark)
     (?y . avy-action-copy)
     (?p . avy-action-yank)
     (?P . avy-action-yank-line)
     (?s . avy-action-ispell)
     (?z . avy-action-zap-to-char)))
  (avy-all-windows t)

  :config
  ;; https://karthinks.com/software/avy-can-do-anything/
  (defun avy-action-embark (pt)
    (unwind-protect
        (save-excursion
          (goto-char pt)
          (embark-act))
      (select-window
       (cdr (ring-ref avy-ring 0))))
    t)
  (setf (alist-get ?. avy-dispatch-alist) 'avy-action-embark))

(use-package avy-embark-collect
  :defer t

  :general
  ( :keymaps '(embark-collect-mode-map minibuffer-local-completion-map)
    "M-i" 'avy-embark-collect-choose
    "M-m" 'avy-embark-collect-act))

(use-package asm-mode
  :defer t

  :gfhook ('asm-mode-set-comment-hook #'my:asm-mode/set-comment-char-from-dir-local-variables)

  :config
  (defun my:asm-mode/set-comment-char-from-dir-local-variables ()
    "Load `asm-comment-char' from dir-local variables.
This is a HACK, since in `asm-mode', `run-mode-hooks' (which would call
`hack-local-variables') is only invoked after the `define-derived-mode' body,
i.e., too late."
    (let ((enable-local-variables :safe))
      (hack-dir-local-variables)
      (when-let* ((char (alist-get 'asm-comment-char dir-local-variables-alist)))
        (setq-local asm-comment-char char)))))

(use-package bibtex
  :defer t

  :custom
  (bibtex-dialect 'biblatex)
  (bibtex-user-optional-fields nil)
  (bibtex-entry-format
   '(opts-or-alts required-fields numerical-fields
                  whitespace last-comma realign
                  delimiters unify-case sort-fields))
  (bibtex-autokey-year-length 4)
  (bibtex-autokey-titlewords 1)
  (bibtex-autokey-titlewords-stretch 0)
  (bibtex-autokey-titleword-length 'infty)
  (bibtex-autokey-name-year-separator "_")
  (bibtex-autokey-year-title-separator "_")
  ;; Set to `nil' as else `bibtex-validate' will complain, which is annoyingly
  ;; called by `org-cite'.
  (bibtex-maintain-sorted-entries nil)
  (bibtex-align-at-equal-sign t)
  (bibtex-comma-after-last-field t)
  (bibtex-unify-case-function #'downcase)

  :config
  (add-to-list 'bibtex-include-OPTcrossref "InReference"))

(use-package bicycle
  :defer t

  :general
  ( :keymaps '(outline-minor-mode-map outline-mode-map)
    "C-<tab>" 'bicycle-cycle
    "S-<tab>" 'bicycle-cycle-global
    "<backtab>" 'bicycle-cycle-global))

(use-package browse-url
  :defer t

  :config
  (when (executable-find "termux-open-url")
    (setq browse-url-browser-function #'browse-url-generic
          browse-url-generic-program "termux-open-url")))

(use-package calc
  :defer t

  :custom
  (calc-group-digits t)
  (calc-group-char "'")
  (calc-context-sensitive-enter t))

(use-package calc-units
  :defer t

  :custom
  (math-additional-units
   '((bit    nil            "Bit")
     (byte   "8 * bit"      "Byte")
     (B      "byte"         "Byte")
     (KiB    "1024 * B"     "Kibibyte")
     (MiB    "1024 ^ 2 * B" "Mebibyte")
     (GiB    "1024 ^ 3 * B" "Gibibyte")
     (TiB    "1024 ^ 4 * B" "Tebibyte")
     (PiB    "1024 ^ 5 * B" "Pebibyte")
     (bps    "bit / s"      "Bit per second"))))

(use-package calendar
  :defer t

  :custom
  (calendar-week-start-day 1)
  (calendar-date-style 'european)
  (calendar-mark-holidays-flag t)

  (calendar-intermonth-text
   '(propertize
     (format "%2d|"
             (car
              (calendar-iso-from-absolute
               (calendar-absolute-from-gregorian
                (list month day year)))))
     'font-lock-face 'calendar-week-face))

  (calendar-day-name-array
   ["Sonntag" "Montag" "Dienstag" "Mittwoch"
    "Donnerstag" "Freitag" "Samstag"])
  (calendar-month-name-array
   ["Januar" "Februar" "März" "April" "Mai" "Juni"
    "Juli" "August" "September" "Oktober" "November" "Dezember"])

  :functions
  calendar-day-of-week
  calendar-gregorian-from-absolute

  :config
  (defface calendar-week-face '((t :inherit font-lock-preprocessor-face))
    "Face used for week numbers in the calendar."
    :group 'calendar-faces)

  (evil-set-initial-state 'calendar-mode 'emacs)
  (add-hook 'calendar-today-visible-hook 'calendar-mark-today)

  (defun calendar-last-workday-of-month+ (month year &optional offset)
    (let* ((day (calendar-last-day-of-month month year))
           (absdate (+ (calendar-absolute-from-gregorian (list month day year))
                       (or offset 0)))
           (workweek '(1 2 3 4 5)))
      (cl-loop for date = (calendar-gregorian-from-absolute absdate)
               until (memq (calendar-day-of-week date) workweek)
               do (cl-decf absdate)
               finally return date)))

  (defun diary-last-workday-of-month (date)
    (let* ((year (calendar-extract-year date))
           (month (calendar-extract-month date)))
      (equal date (calendar-last-workday-of-month+ month year))))

  (defun diary-day-before-last-workday-of-month (date)
    (let* ((year (calendar-extract-year date))
           (month (calendar-extract-month date)))
      (equal date (calendar-last-workday-of-month+ month year -1)))))

(use-package cc-mode
  :defer t
  :mode ("\\.h\\'" . c++-mode)

  :general-config
  (:keymaps 'c-mode-base-map
            "C-m" 'newline-and-indent
            [ret] 'newline-and-indent)

  :custom
  (c-default-style
   '((java-mode . "java")
     (awk-mode . "awk")
     (other . "llvm.org")))
  ;; Insert spaces according to tab-width when not at BOL
  (c-tab-always-indent nil)
  ;; (c-insert-tab-function 'insert-tab)
  (c-insert-tab-function (lambda () (insert-char ?\s c-basic-offset)))

  :functions
  c-setup-filladapt
  c-toggle-auto-newline
  c-toggle-electric-state

  :config
  (defalias 'cpp-mode 'c++-mode)

  (setq-default c-doc-comment-style
                '((java-mode . javadoc)
                  (pike-mode . autodoc)
                  (c-mode . doxygen)
                  (c++-mode . doxygen)))

  (defun hooks:c-mode-setup ()
    (when (string-match "llvm" (or buffer-file-name ""))
      (c-set-style "llvm.org"))
    (doxygen-mode 1)
    (and (featurep 'filladapt)
         (boundp 'filladapt-token-table)
         (c-setup-filladapt))
    (c-toggle-auto-newline 1)
    (c-toggle-electric-state 1))
  (add-hook 'c-mode-common-hook #'hooks:c-mode-setup)

  (c-add-style "llvm.org"
               '("gnu"
                 (fill-column . 80)
                 (c++-indent-level . 2)
                 (c-basic-offset . 2)
                 (indent-tabs-mode . nil)
                 (c-offsets-alist . ((arglist-intro . ++)
                                     (innamespace . 0)
                                     (member-init-intro . ++))))))

(use-package citar
  :defer t

  :general
  (:keymaps 'org-mode-map
            ;; match binding for citation `C-c (' used in reftex-mode
            "C-c (" #'citar-insert-citation)

  :custom
  (citar-at-point-function 'embark-act)
  (citar-library-paths '("~/read/"))
  (citar-notes-paths '("~/zettelkasten/bib/"))
  (citar-bibliography '("~/read/read.bib"))
  (org-cite-activate-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-insert-processor 'citar)

  :config
  (setf (alist-get 'note citar-templates)
        "Notes on “${shorttitle title}” (${author editor:%etal})\n#+language: en"))

(use-package citar-embark
  :after citar embark
  :diminish ""
  :no-require
  :config (citar-embark-mode))

(use-package clang-format
  :defer t

  :general
  (general-nvmap :keymaps 'c-mode-base-map "zq" 'evil-clang-format)

  :init
  (evil-define-operator evil-clang-format (beg end)
    "Format text from BEG to END according to STYLE."
    :move-point nil
    (clang-format-region beg end)))

(use-package cmake-ts-mode
  :if (treesit-ready-p 'cmake 'quiet)
  :defer t

  :mode ((rx (| "CMakeLists.txt" ".cmake") eos) . cmake-ts-mode))

(use-package compile
  :defer t
  :general
  (my:general/leader
   "r" 'recompile
   "R" 'compilation-display-last-buffer+)

  :custom
  (compilation-environment '("TERM=eterm-color"))
  (compilation-scroll-output 'first-error)
  (compilation-skip-threshold 1)        ; skip info/note messages

  :config
  (when (fboundp 'ansi-osc-compilation-filter)
    (add-hook 'compilation-filter-hook 'ansi-osc-compilation-filter))
  (when (fboundp 'ansi-color-compilation-filter)
    (add-hook 'compilation-filter-hook #'ansi-color-compilation-filter))

  (defun hooks:compilation-mode-setup ()
    (hl-line-mode 1)
    (setq truncate-lines nil)
    (setq word-wrap t))
  (add-hook 'compilation-mode-hook #'hooks:compilation-mode-setup)

  (defun compilation-display-last-buffer+ ()
    "Display the most recent compilation buffer."
    (interactive)
    (when (boundp 'compilation-last-buffer)
      (display-buffer compilation-last-buffer)))

  (define-advice compile-goto-error (:before (&rest _args) set-marker)
    "Set the marker from where the next error will be found."
    (setq compilation-current-error (point-marker))))

(use-package compile-in-dir
  :defer t
  :commands compile-in-dir-with-dominating-file)

(use-package conf-mode
  :defer t

  :mode (("\\.toml\\'" . conf-toml-mode)
         ("Cargo\\.lock\\'" . conf-toml-mode)
         ("\\.desktop\\'" . conf-desktop-mode)))

(use-package consult
  :defer t

  :general
  ;; `ctl-x-map' (C-x)
  ("C-x M-:" 'consult-complex-command)
  ("C-x b" 'consult-buffer)
  ("C-x 4 b" 'consult-buffer-other-window)
  ("C-x 5 b" 'consult-buffer-other-frame)
  ;; `goto-map' (M-g)
  ("M-g u" 'first-error)
  ("M-g n" 'next-error)
  ("M-g p" 'previous-error)
  ("M-g e" 'consult-compile-error)
  ("M-g f" 'consult-flymake)
  ("M-g g" 'consult-goto-line)
  ("M-g M-g" 'consult-goto-line)
  ("M-g o" 'consult-outline)
  ("M-g m" 'consult-mark)
  ("M-g k" 'consult-global-mark)
  ;; `search-map' (M-s)
  ("M-s l" 'consult-line)
  ("M-s m" 'consult-line-multi)
  ("M-s k" 'consult-keep-lines)
  ("M-s v" 'consult-focus-lines)
  ("M-s g p" 'consult-grep)
  ("M-s g g" 'consult-git-grep)
  ("M-s g r" 'consult-ripgrep)
  ("M-s e" 'consult-isearch-history)
  (:keymaps 'kmacro-keymap
            "k" 'consult-kmacro)
  (:keymaps 'isearch-mode-map
            "M-e" 'consult-isearch-history
            "M-s e" 'consult-isearch-history
            "M-s l" 'consult-line
            "M-s L" 'consult-line-multi)
  (:keymaps '(compilation-minor-mode-map compilation-mode-map)
            "e" 'consult-compile-error)
  (:keymaps 'minibuffer-local-map
            "M-s" 'consult-history
            "M-r" 'consult-history)
  ("C-c y" 'consult-yank-replace)
  (my:general/leader
    "' b" 'consult-bookmark
    "' '" 'consult-register
    "' l" 'consult-register-load
    "' s" 'consult-register-store
    "g b" 'consult-buffer
    "g e" 'consult-compile-error
    "g g" 'consult-git-grep
    "g r" 'consult-recent-file
    "g f" 'find-file
    "g i" 'consult-imenu
    "g p" 'consult-imenu-multi)

  :custom
  (register-preview-delay 0)
  (register-preview-function #'consult-register-format)
  (xref-show-xrefs-function #'consult-xref)
  (xref-show-definitions-function #'consult-xref)

  :config
  (advice-add #'register-preview :override #'consult-register-window)

  (consult-customize
   consult-theme
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   :preview-key '(:debounce 0.4 any))

  (define-advice consult-completion-in-region
      (:around (fun &rest args) fix-exit-function)
    "Do not run exit function twice when completing in region.

`minibuffer-force-complete' calls `completion--done' inside the
minibuffer.  This has the effect of prematurely running the
:exit-function, which in the case of eglot has an effect on the
original buffer.  As `consult-completion-in-region' already takes
care of running the :exit-function, disable `completion--done' in
this case."
    ;; Only apply this workaround in eglot-managed buffers for now.
    (if (and (fboundp 'eglot-managed-p)
             (eglot-managed-p))
        (cl-letf* (((symbol-function 'completion--done) #'ignore))
          (apply fun args))
      (apply fun args)))

  (with-eval-after-load 'projectile
    (setq consult-project-function (lambda (_may-prompt) (projectile-project-root)))))

(use-package corfu
  :defer t

  :ghook
  ('prog-mode-hook #'corfu-mode)

  :general-config
  (:keymaps 'corfu-map
            [remap evil-complete-next] 'corfu-next
            [remap evil-complete-previous] 'corfu-previous
            "TAB" 'corfu-next
            [tab] 'corfu-next
            ;; M-TAB is bound to `corfu-expand' by default, but
            ;; `corfu-insert-separator' is more useful together with orderless.
            "M-TAB" 'corfu-insert-separator
            "S-TAB" 'corfu-previous
            [backtab] 'corfu-previous)

  :custom
  (corfu-auto t)
  (corfu-auto-delay 1.0)
  (corfu-cycle t)
  (corfu-quit-no-match 'separator)
  (corfu-echo-mode t)

  :config
  (with-eval-after-load 'evil-core
    (add-to-list 'evil-buffer-regexps
                 '("\\` \\*corfu\\(-doc\\)?\\*" . nil))))

(use-package corfu-terminal
  :ensure t
  :after corfu

  :config
  (corfu-terminal-mode))

(use-package crm
  :defer t

  :config
  (define-advice completing-read-multiple
      (:filter-args (args) add-indicator-to-prompt)
    (let ((to-trim (regexp-quote "[ \t]*")))
    (cons (format "⟨%s⟩ %s"
      (string-trim crm-separator
                   to-trim to-trim)
                  (car args))
          (cdr args)))))

(use-package demangle-mode
  :defer t
  :hook llvm-mode compilation-mode compilation-minor-mode)

(use-package devdocs
  :defer t

  :general-config
  (:keymaps 'devdocs-mode-map
            "g" 'revert-buffer
            "G" 'devdocs-goto-page)

  :custom
  (devdocs-data-dir (no-littering-expand-var-file-name "devdocs"))

  :defines
  devdocs-cdn-url
  devdocs-site-url

  :init
  (defun devdocs-install-gnome+ ()
    (interactive)
    (let ((devdocs-site-url "https://gjs-docs.gnome.org")
          (devdocs-cdn-url "https://gjs-docs.gnome.org/docs"))
      (call-interactively 'devdocs-install))))

(use-package diff
  :defer t
  :custom (diff-font-lock-prettify t))

(use-package dired
  :defer t

  :hook (dired-mode . dired-hide-details-mode)

  :general
  (my:general/leader "-" 'dired-jump)
  :general-config
  ( :keymaps 'dired-mode-map
    "-" 'dired-jump)

  :custom
  (dired-vc-rename-file t)
  (dired-dwim-target t)
  (dired-listing-switches "-alvh")
  (dired-kill-when-opening-new-dired-buffer nil)

  :config
  (evil-set-initial-state 'dired-mode 'emacs)
  (put 'dired-find-alternate-file 'disabled nil)

  (defun hooks:dired-mode-setup ()
    (hl-line-mode)
    (setq mode-line-buffer-identification
          (default-value 'mode-line-buffer-identification)))
  (add-hook 'dired-mode-hook 'hooks:dired-mode-setup))

(use-package dired-git-info
  :defer t

  :general-config
  (:keymaps 'dired-mode-map
            "M-)" 'dired-git-info-mode))

(use-package dired-x
  :defer t

  :hook (dired-mode . dired-omit-mode)
  :general-config
  ( :keymaps 'dired-mode-map
    ")" 'dired-omit-mode)

  :custom
  (dired-enable-local-variables nil)

  :config
  (dolist (ext '(".bcf" ".fff" ".rel" ".run.xml" ".synctex.gz"))
    (add-to-list 'dired-omit-extensions ext)))

(use-package disp-table
  :defer t

  :config
  (set-display-table-slot
   standard-display-table
   'selective-display
   (cl-map
    'vector
    (lambda (c)
      (make-glyph-code c 'font-lock-keyword-face))
    " ··· ")))

(use-package dogears
  :defer 0.5

  :general
  ( "M-g M-g" 'consult-dogears-go+
    "M-g M-h" 'dogears-back
    "M-g M-f" 'dogears-forward
    "M-g d" 'dogears-list
    "M-g M-d" 'dogears-sidebar-toggle+)

  :custom
  (dogears-idle (* 5 60))
  (dogears-limit 200)
  (dogears-position-delta 30)
  (dogears-functions
   '(evil-scroll-line-to-center
     nov-visit-relative-file
     recenter-top-bottom))
  (dogears-hooks
   '(imenu-after-jump-hook
     find-function-after-hook
     ;; xref-after-jump-hook
     ;; xref-after-return-hook
     org-follow-link-hook
     org-clock-in-hook
     org-clock-out-hook))
  (dogears-sidebar-alist
   `(,@(if (my:frame-wide-p)
           '((side . right)
             (slot . 3)
             (window-width . 60))
         '((side . top)
           (window-height . 10)))
     (window-parameters
      ;; preserve width when resizing (other) windows
      (preserve-size . (t . nil))
      (no-delete-other-windows . t)
      ;; instead use `dogears-list' to switch to window
      (no-other-window . t))))

  :config
  (defun my:dogears/ignore-place-p ()
    (derived-mode-p '(magit-mode)))
  (add-to-list 'dogears-ignore-places-functions #'my:dogears/ignore-place-p)

  (defun my:dogears/list-jump-to-position (&rest _)
    (when (and dogears-update-list-buffer (buffer-live-p dogears-list-buffer))
      (with-selected-window (get-buffer-window dogears-list-buffer)
        (goto-line (1+ dogears-position)))))

  ;; TODO: `dogears-go' does not set `dogears-position'… Intentional?
  (advice-add 'dogears-back :after 'my:dogears/list-jump-to-position)
  (advice-add 'dogears-forward :after 'my:dogears/list-jump-to-position)

  (defun dogears-sidebar-toggle+ ()
    "Toggle display of the Dogears list in a side window."
    (interactive)
    (if-let* ((buffer (get-buffer "*Dogears List*"))
              (win (get-buffer-window buffer)))
        (quit-window nil win)
      (dogears-sidebar)))

  (defun consult-dogears-go+ (&optional all-places)
    "Like `dogears-go' but with preview."
    (interactive "P")
    (let ((places dogears-list) bookmark-fringe-mark)
      (unless all-places
        (setq places (cl-remove-if 'string-empty-p places :key 'dogears--relevance)))
      ;; TODO: Is there a similar public interface?
      (consult--read
       (cl-loop
        for i from 0 for place in places collect
        ;; TODO: This is portable, but maybe there is a faster way…?
        (save-excursion
          (bookmark-jump place)
          (let ((cand (format "%3.2s: %s" i (dogears--format-record place)))
                (marker (point-marker))
                (line (line-number-at-pos nil consult-line-numbers-widen)))
            (put-text-property
             0 (length cand) 'consult-location (cons marker line) cand)
            ;; TODO: The “future history” available via M-n is useless, since
            ;; it consists of  the complete formatted record…
            cand)))
       :prompt "Place: "
       :category 'consult-location
       :sort nil
       :require-match t
       :lookup #'consult--lookup-location
       :state (consult--jump-state))))

  (define-advice xref-push-marker-stack (:after (&optional m) dogear-before-jump)
    "Dogear point before jumping, as there is no `xref-before-jump-hook'."
    (save-excursion
      (when-let* ((m) (buf (marker-buffer m)))
        (set-buffer buf)
        (goto-char (marker-position m)))
      (dogears-remember)))

  ;; Dogear point after jumping, unfortunately we cannot use
  ;; `xref-after-jump-hook', since that is called _before_
  ;; `xref-push-marker-stack', i.e., too early.
  (advice-add 'xref--push-markers :after 'dogears-remember)
  (dogears-mode))

(use-package doxygen
  :defer t
  :commands doxygen-mode)

(use-package ediff
  :defer t

  :custom
  (ediff-split-window-function 'split-window-horizontally)
  (ediff-window-setup-function 'ediff-setup-windows-plain)

  :config
  (with-eval-after-load 'evil-core
    (add-to-list 'evil-buffer-regexps
                 (cons
                  (rx bos "*" (| "Ediff File Group Differences"
                                 "Ediff Multifile Diffs")
                      "*")
                  'emacs))))

(use-package eglot
  :defer t

  :general-config
  (general-nvmap :keymaps 'eglot-mode-map
    "zq" 'evil-eglot-format
    "zn" 'eglot-rename)
  (general-mmap :keymaps 'eglot-mode-map
    "gh" 'eglot-inlay-hints-mode
    "g!" 'eglot-code-actions
    "gor" 'xref-find-references
    "god" 'xref-find-definitions
    "goD" 'xref-find-definitions-other-window
    "goh" 'eglot-find-declaration
    "goi" 'eglot-find-implementation
    "got" 'eglot-find-typeDefinition
    [remap evil-goto-definition] 'xref-find-definitions
    [remap evil-jump-to-tag] 'xref-find-apropos)

  :functions
  eglot-inlay-hints-mode
  :gfhook
  ('eglot-managed-mode-hook #'eglot-inlay-hints-mode)

  :custom
  (eglot-extend-to-xref t)
  (eglot-events-buffer-config '(:size 0 :format short))

  :config
  (define-advice minibuffer-force-complete
      (:filter-args (&optional args) fix-eglot-cycling-bounds)
    "When setting up cyclic completion, `minibuffer-force-complete'
  sets up a transient key map that makes use of the the position of
  point after the last completion.  It uses that position as the
  end bound for the region to delete before inserting the next
  candidate.  Unfortunately eglot's :exit-function does not leave
  point at the end of the inserted text in case of a snippet.
  E.g. when completing a function call not the complete function
  call is deleted when cycling to the next completion, but only the
  name of the function and the opening parenthesis.

  This advice derives the `end' position from the most recently
  inserted snippet overlapping with the originally cycled range
  in that specific case."
    (if (and (eql 'completion-at-point last-command)
             completion-cycling
             (boundp 'yas-minor-mode) (symbol-value 'yas-minor-mode))
      (cl-destructuring-bind (&optional start end dont-cycle) args
        (when-let ((snippets (yas-active-snippets start end)))
          (setq end (overlay-end (yas--snippet-control-overlay (car snippets)))))
        (apply 'list start end dont-cycle))
      args))

  (evil-define-operator evil-eglot-format (beg end)
    "Format text from BEG to END using eglot or clang-format."
    :move-point nil
    (cond
     ((ignore-errors (eglot-format beg end)))
     ((and
       (bound-and-true-p c-buffer-is-cc-mode)
       (fboundp 'clang-format-region)
       (clang-format-region beg end)))))

  (setf
   (alist-get '(c++-mode c-mode) eglot-server-programs nil nil 'equal)
   '("clangd"
     "--all-scopes-completion"
     "--background-index"
     "--clang-tidy"
     "--completion-style=detailed"
     "--function-arg-placeholders"
     "--header-insertion=iwyu"
     "--header-insertion-decorators"
     "--enable-config"))

  (setf
   (alist-get '(rust-ts-mode rust-mode) eglot-server-programs nil nil 'equal)
   '("rust-analyzer" :initializationOptions (:check (:command "clippy")))))

(use-package eldoc
  :defer t
  :diminish ""

  :custom
  (eldoc-echo-area-use-multiline-p 5)
  (eldoc-echo-area-prefer-doc-buffer 'maybe)
  (eldoc-echo-area-display-truncation-message nil)

  :config
  (with-eval-after-load 'evil-core
    (add-to-list 'evil-buffer-regexps '("\\`\\*eldoc\\*" . emacs))))

(use-package electric
  :defer t

  :ghook
  ('c-ts-base-mode-hook #'my:electric/c-like-setup)
  ('rust-ts-mode-hook #'my:electric/c-like-setup)

  :custom
  (electric-indent-mode t)
  (electric-layout-mode t)

  :config
  (defun my:electric/c-like-setup ()
    (setq-local electric-layout-rules
                '((?\; . after) (?\{ . after) (?\} . before)))))

(use-package elec-pair
  :defer t

  :custom
  (electric-pair-skip-whitespace 'chomp)
  (electric-pair-mode t))

(define-advice elisp-get-fnsym-args-string
    (:around (fun sym &rest rest) add-first-line-of-docstring)
  (concat
   (apply fun sym rest)
   (let* ((doc (or (ignore-errors (documentation sym 'raw)) ""))
          (first-line (substring doc 0 (string-match "\n" doc))))
     (unless (string-empty-p first-line)
       (concat
        "   "
        (propertize (concat "\"" first-line "\"")
                    'face 'font-lock-doc-face))))))

(use-package embark
  :defer t

  :general
  ( :keymaps '(minibuffer-local-completion-map vertico-map)
    "M-o" 'embark-export)
  ( :keymaps 'embark-file-map
    "-" 'embark-dired-jump)
  ("C-c j" 'embark-act)
  ("C-c ." 'embark-dwim)
  ("C-c C-j" 'embark-act)

  :custom
  (embark-cycle-key "<DEL>"))

(use-package embark-consult
  :after (embark consult)
  :demand t

  :ghook ('embark-collect-mode-hook #'consult-preview-at-point-mode))

(use-package envrc
  :demand t

  :hook (after-init . envrc-global-mode)

  :custom
  (envrc-none-lighter '(" env[" (:propertize "?" face envrc-mode-line-none-face) "]"))
  (envrc-on-lighter '(" env[" (:propertize "✔" face envrc-mode-line-on-face) "]"))
  (envrc-error-lighter '(" env[" (:propertize "✘" face envrc-mode-line-error-face) "]")))

(use-package evil-surround
  :demand t

  :custom
  (global-evil-surround-mode t))

(use-package evil-args
  :defer t

  :general
  (:keymaps 'evil-inner-text-objects-map "a" 'evil-inner-arg)
  (:keymaps 'evil-outer-text-objects-map "a" 'evil-outer-arg))

(use-package evil-commentary
  :demand t
  :diminish evil-commentary-mode

  :custom
  (evil-commentary-mode t))

(evil-define-operator evil-paste-replacing (beg end type register yank-handler)
  "Delete text from BEG to END with TYPE and insert contents of REGISTER."
  (evil-delete beg end type ?_ yank-handler)
  (evil-paste-before 1 register))

(general-nvmap "gx" 'evil-paste-replacing)

(defun my:rotate-case-style (word)
  "Convert WORD to a different case style."
  (let ((case-fold-search nil)
        (snake-case-rx (rx bos (+ (any lower num "_")) eos))
        (macro-case-rx (rx bos (+ (any upper num "_")) eos))
        (lower-camel-case-rx (rx bos (any lower) (* (any lower upper num))
                                 (any upper) (* (any lower upper num)) eos)))
    (cond ((string-match snake-case-rx word)
           (upcase word))
          ((string-match macro-case-rx word)
           (s-lower-camel-case word))
          ((string-match lower-camel-case-rx word)
           (s-upper-camel-case word))
          (t (s-snake-case word)))))

(defun rotate-case-style-at-point+ ()
  "Convert symbol at point to a different case style."
  (interactive)
  (when-let* ((bounds (bounds-of-thing-at-point 'symbol))
              (prev-point (point))
              (text (filter-buffer-substring (car bounds) (cdr bounds) 'delete)))
    (insert (my:rotate-case-style text))
    ;; Stick to end, else approximately jump to previous point
    (unless (eq prev-point (cdr bounds))
      (goto-char prev-point))))

(general-define-key "M-c" 'rotate-case-style-at-point+)

(evil-define-operator evil-inflect (beg end _type)
  "Convert text between BEG and END to a different case style."
  (let ((text (filter-buffer-substring beg end 'delete)))
    (save-excursion
      (insert (my:rotate-case-style text)))))

(general-nvmap "g~" 'evil-inflect)

(defun get-common-indent-in-region (start end)
  "Return the minimum number of leading white space for the nonempty lines between START and END."
  (let (indentation)
    (save-excursion
      (goto-char start)
      (or (bolp) (forward-line 1))
      (while (< (point) end)
        (skip-chars-forward " \t")
        (unless (eolp)
          (setq indentation
                (if indentation
                    (min (current-column) indentation)
                  (current-column))))
        (forward-line 1)))
    (or indentation 0)))

(defun evil-adjust-indent-afer-paste+ (&optional command)
  "Adjust the indent of the preceding paste COMMAND to match the indentation of the adjacent line."
  (interactive (list last-command))

  (unless (memq command
                '(evil-paste-after
                  evil-paste-before))
    (error "Previous command was not a line-wise evil-paste: %s" command))

  (let* ((start (evil-get-marker ?\[))
         (end (evil-get-marker ?\]))
         (common (get-common-indent-in-region start end))
         (this
          (save-excursion
            (cond
             ((eql command 'evil-paste-before)
              (goto-char end)
              (forward-line 1))
             (t
              (goto-char start)
              (forward-line -1)))
            (skip-chars-forward " \t")
            (current-column))))
    (indent-rigidly start end (- this common))))

(evil-define-command evil-paste-before-adjusting-indent+
  (count &optional register yank-handler)
  "Like `evil-paste-before' but adjusts the indent to the current line."
  :suppress-operator t
  (interactive "P<x>")
  (evil-with-single-undo
    (evil-paste-before count register yank-handler)
    (evil-adjust-indent-afer-paste+ 'evil-paste-before)))

(evil-define-command evil-paste-after-adjusting-indent+
  (count &optional register yank-handler)
  "Like `evil-paste-after' but adjusts the indent to the current line."
  :suppress-operator t
  (interactive "P<x>")
  (evil-with-single-undo
    (evil-paste-after count register yank-handler)
    (evil-adjust-indent-afer-paste+ 'evil-paste-after)))

(general-nmap "]p" 'evil-paste-after-adjusting-indent+)
(general-nmap "]P" 'evil-paste-before-adjusting-indent+)
(general-nmap "[p" 'evil-paste-before-adjusting-indent+)
(general-nmap "[P" 'evil-paste-before-adjusting-indent+)

(use-package executable
  :no-require t

  :custom
  (executable-prefix-env t))

(use-package expreg
  :defer t

  :general
  (general-nvmap
    "g+" 'expreg-expand
    "g-" 'expreg-contract))

(use-package face-remap
  :defer t

  :custom
  (text-scale-mode-step 1.1))

(use-package files
  :defer t

  :custom
  (view-read-only t)
  (require-final-newline 'ask)
  (backup-directory-alist
   `(("\\`/tmp/" . nil)
     ("\\`/dev/shm/" . nil)
     ("." . ,(no-littering-expand-var-file-name "backup/"))))
  (enable-local-variables :safe)
  (enable-local-eval nil))

(use-package fill
  :defer t

  :custom
  (fill-nobreak-predicate '(fill-single-char-nobreak-p
                            fill-single-word-nobreak-p)))

(use-package filladapt
  :defer t
  :diminish "FA"

  :custom
  (filladapt-fill-column-tolerance 6))

(use-package find-file
  :defer t
  :config
  (add-to-list 'cc-search-directories "/usr/include/c++/*" 'append))

(use-package flymake
  :defer t

  :general
  (general-nmap
    "] f" 'flymake-goto-next-error
    "[ f" 'flymake-goto-prev-error)
  (my:general/leader
    "g d" 'avy-flymake-diagnostics+)

  :custom
  (flymake-show-diagnostics-at-end-of-line 'short)

  :config
  (define-advice elisp-flymake-byte-compile
      (:around (fun &rest args) use-current-load-path)
    "Use current value of `load-path' when byte-compiling files."
    (cl-letf* ((make-process-fn (symbol-function 'make-process))
               ((symbol-function 'make-process)
                (lambda (&rest process-args)
                  (let* ((command (plist-get process-args :command))
                         (split-list (-split-on "-L" command)))
                    ;; Remove all -L arguments and splice in new load path.
                    (setq command (apply 'nconc (car split-list)
                                         (-mapcat (lambda (x) (list "-L" x)) load-path)
                                         (mapcar 'cdr (cdr split-list))))
                    (setq process-args (plist-put process-args :command command)))
                  (apply make-process-fn process-args))))
      (apply fun args)))

  (with-eval-after-load 'avy
    (defun avy-flymake-diagnostics+ (&optional flip)
      "Jump to currently visible flymake diagnostics.

The window scope is determined by `avy-all-windows' or
`avy-all-windows-alt' if FLIP is non-nil."
      (interactive "P")
      (let ((avy-all-windows (if flip avy-all-windows-alt avy-all-windows))
            (avy-background t)
            candidates)
        (dolist (window (avy-window-list))
          (with-selected-window window
            (unless (derived-mode-p avy-ignored-modes)
              (dolist (diag (flymake-diagnostics (window-start)
                                                 (window-end nil 'update)))
                (push (cons (cons (flymake-diagnostic-beg diag)
                                  (flymake-diagnostic-end diag)) window)
                      candidates)))))
        (avy-process candidates)))))

(use-package flyspell
  :defer t

  :general-config
  (:keymaps 'flyspell-mode-map
            "M-TAB" nil)

  :custom
  (flyspell-abbrev-p t)
  (flyspell-use-meta-tab nil)
  (flyspell-use-global-abbrev-table-p nil)

  :config
  (add-to-list 'flyspell-delayed-commands #'expand-abbrev))

(use-package follow
  :defer t

  :hook ((follow-mode-on . (lambda () (setq-local scroll-margin 0)))
         (follow-mode-off . (lambda () (kill-local-variable 'scroll-margin)))))

(use-package footnote
  :defer t

  :custom
  (footnote-section-tag ""))

(use-package gdb-mi
  :defer t

  :custom
  (gdb-many-windows t)
  (gdb-show-main t)
  (gdb-restore-window-configuration-after-quit t))

(use-package git-commit
  :defer t

  :config
  (evil-set-initial-state 'git-commit-mode 'normal)
  (remove-hook 'git-commit-setup-hook 'git-commit-setup-changelog-support)
  (add-hook 'git-commit-setup-hook 'git-commit-turn-on-flyspell 'append)
  (add-to-list 'git-commit-trailers "Change-Id" 'append)
  (add-to-list 'git-commit-trailers "Issue-Id" 'append))

(use-package glasses
  :defer t

  :custom
  (glasses-separator "⋅")
  (glasses-original-separator "")
  (glasses-face nil)
  (glasses-separate-parentheses-p nil))

(use-package gnus
  :defer t

  :custom
  (user-mail-address "johann@jklaehn.de")
  (user-full-name "Johann Klähn")
  (mail-user-agent 'gnus-user-agent)

  (gnus-user-agent nil)
  (gnus-inhibit-startup-message t)
  (gnus-large-newsgroup 1000)
  (gnus-save-newsrc-file nil)
  (gnus-read-newsrc-file nil)

  (gnus-check-new-newsgroups nil)
  ;; (gnus-read-active-file nil)
  ;; (gnus-check-bogus-newsgroups nil)
  (gnus-subscribe-newsgroup-method 'gnus-subscribe-killed)
  (gnus-save-killed-list nil)

  (gnus-use-cache 'passive)
  (gnus-refer-article-method 'current)
  (gnus-refer-thread-use-search t)

  (gnus-secondary-select-methods
   `((nnimap "mail"
             (nnimap-stream shell)
             (nnimap-shell-program "/usr/libexec/dovecot/imap -c ~/.dovecotrc")
             (nnimap-expunge 'never))
     (nntp "dlang"
           (nntp-address "news.digitalmars.com"))
     (nntp "news.gmane.io"
           (nntp-address "news.gmane.io"))))

  (gnus-message-archive-group '((format-time-string "sent.%Y-Q%q")))
  (gnus-gcc-mark-as-read t)

  (gnus-permanently-visible-groups (rx "nnimap+mail:"))
  (gnus-parameters
   `(("nnimap\\+.*"
      (gnus-use-scoring nil))
     ("nnimap\\+mail:.*"
      (expiry-target . "nnimap+mail:Trash")
      (expiry-wait . immediate))
     ("nnimap\\+mail:Trash"
      (expiry-target . delete)
      (expiry-wait . never))
     (,(rx "nntp+news.gmane.io:"
           "gmane.comp.compilers."
           (or "clang.scm" "llvm.cvs"))
      (gnus-subthread-sort-functions
       '(gnus-thread-sort-by-number)))))

  (gnus-extra-headers '(To Cc Keywords Gcc Newsgroups Content-Type List-ID))
  (nnmail-extra-headers '(To Cc Keywords Gcc Newsgroups Content-Type List-ID))

  (gnus-home-directory (expand-file-name "~/sync/gnus/"))
  (gnus-directory (expand-file-name "news/" gnus-home-directory))
  (gnus-startup-file (expand-file-name "newsrc" gnus-home-directory))
  (gnus-article-save-directory gnus-directory)
  (gnus-kill-files-directory gnus-directory)
  (gnus-cache-directory (expand-file-name "cache/" gnus-directory))
  (gnus-cache-active-file (expand-file-name "active" gnus-cache-directory))
  (gnus-message-archive-method
  `(nnfolder "archive"
             (nnfolder-directory ,(expand-file-name "archive/" gnus-home-directory))
             (nnfolder-active-file ,(expand-file-name "archive/active" gnus-home-directory))
             (nnfolder-inhibit-expiry t)
             (nnfolder-get-new-mail nil)))
  (gnus-update-message-archive-method t)

  :defines
  gnus-face-1

  :config
  ;; KLUDGE: `no-littering' unconditionally sets `gnus-startup-file',
  ;; overwriting our customization.
  (defun my:gnus/fix-startup-file ()
    (setq gnus-startup-file
          (expand-file-name "newsrc" gnus-home-directory)))
  (add-hook 'gnus-before-startup-hook 'my:gnus/fix-startup-file)

  (defun my:gnus/exit-gnus-on-exit ()
    (if (and (fboundp 'gnus-group-exit)
             (gnus-alive-p))
        (with-current-buffer (get-buffer "*Group*")
          (let (gnus-interactive-exit)
            (gnus-group-exit)))))
  (add-hook 'kill-emacs-hook 'my:gnus/exit-gnus-on-exit)

  ;; Email from Juan José García-Ripoll: use-package :custom causes gnu (Sat, 28 Oct 2023 11:53:47 +0200)
  ;; gnus:nntp+news.gmane.io:gmane.emacs.gnus.user#86cywzhy38.fsf@csic.es
  (setq gnus-select-method '(nnnil ""))

  (defvar gnus-face-9)
  (defvar gnus-face-10)
  (setq gnus-face-1 'italic
        gnus-face-9 'font-lock-warning-face
        gnus-face-10 'shadow))

(use-package gnus-dbus
  :defer t
  :custom
  (gnus-dbus-close-on-sleep t))

(use-package gnus-cus
  :defer t
  :config
  (evil-set-initial-state 'gnus-custom-mode 'emacs))

(defun my:gnus/prefer-horizontal-p ()
  (and (my:frame-landscape-p)
       (or (my:frame-small-p)
           (my:frame-wide-p))))

(use-package gnus-win
  :defer t

  ;; :custom
  ;; (setq gnus-use-atomic-windows t)

  :config
  (gnus-add-configuration
   '(article
     (let ((prefer-horizontal (my:gnus/prefer-horizontal-p)))
       (cond
        ((and gnus-use-trees prefer-horizontal)
         '(horizontal 1.0
                      (vertical 0.5
                                (summary 1.0 point)
                                (tree 0.5))
                      (vertical 1.0 (article 1.0))))
        (prefer-horizontal
         '(horizontal 1.0
                      (vertical 0.5 (summary 1.0 point))
                      (vertical 1.0 (article 1.0))))
        (gnus-use-trees
         '(vertical 1.0
                    (summary 0.25 point)
                    (tree 0.25)
                    (article 1.0)))
        (t
         '(vertical 1.0
                    (summary 0.3 point)
                    (article 1.0))))))))

(use-package gnus-score
  :defer t

  :custom
  (gnus-use-adaptive-scoring '(line))
  (gnus-score-interactive-default-score 100)
  (gnus-score-default-duration nil)
  (gnus-score-expiry-days 30)

  (gnus-decay-scores "\\.ADAPT\\'")
  (gnus-score-decay-constant 2)
  (gnus-score-decay-scale 0.05)

  (gnus-default-adaptive-score-alist
   '((gnus-saved-mark     (subject 10)   (from 10))
     (gnus-forwarded-mark (subject 10)   (from 10))
     (gnus-replied-mark   (subject 10)   (from 5))
     (gnus-read-mark      (subject 5)    (from 1))
     (gnus-ticked-mark)
     (gnus-del-mark       (subject -5))
     (gnus-catchup-mark   (subject -10))
     (gnus-killed-mark    (subject -15))
     (gnus-expirable-mark (subject -100))
     (gnus-spam-mark      (subject -100) (from -50))))

  (gnus-score-find-score-files-function 'gnus-score-find-hierarchical)

  :config
  (with-eval-after-load 'evil-core
    (add-to-list 'evil-buffer-regexps '("\\`\\*Score Trace\\*" . emacs)))

  (defun my:gnus/score-group-p (&optional group)
    (and gnus-use-scoring
         (string-match (rx bos "nntp+news.gmane.io:")
                       (or group gnus-newsgroup-name ""))))

  (define-advice gnus-score-adaptive (:around (orig-fun &rest args) only-conditionally)
    (when (my:gnus/score-group-p)
      (apply orig-fun args))))

(use-package gnus-registry
  :disabled
  :after gnus
  :demand t

  :custom
  (gnus-registry-register-all nil)
  (gnus-registry-max-entries nil)

  :config
  (add-to-list 'gnus-registry-ignored-groups '("^nntp" t))
  (gnus-registry-initialize))

(use-package mm-uu
  :no-require t

  :custom
  ;; cf. `mm-uu-type-alist'
  (mm-uu-configure-list '((shar . disabled)
                          (org-meta-line . disabled))))

(use-package gnus-art
  :defer t

  :general-config
  (:keymaps 'gnus-article-mode-map
            "DEL" 'gnus-article-goto-prev-page-or-expand-summary+)

  :gfhook ('gnus-article-mode-hook #'my:gnus/setup-article-mode)

  :custom
  (gnus-inhibit-images t)
  (gnus-article-emulate-mime nil)       ; cf. `mm-uu-type-alist'
  (gnus-article-skip-boring t)
  (gnus-article-date-headers '(combined-local-lapsed))
  (gnus-treat-display-face nil)
  (gnus-treat-display-smileys nil)

  :config
  (defun gnus-article-goto-prev-page-or-expand-summary+ (&optional arg)
    "Show the previous page of the article or expand the summary buffer.
If given a prefix argument, always expand the summary buffer."
    (interactive "P")
    (if (or arg (pos-visible-in-window-p (point-min)))
        (gnus-summary-expand-window)
      (gnus-article-prev-page nil)))

  (add-to-list 'gnus-buttonized-mime-types "multipart/alternative")
  ;; Expand text/x-patch attachments by default.  See also `mm-inlined-types'.
  (dolist (mime-type '("text/x-patch"))
    (add-to-list 'mm-automatic-display mime-type)
    (add-to-list 'mm-attachment-override-types mime-type)
    (add-to-list 'gnus-buttonized-mime-types mime-type))

  (defun my:gnus/setup-article-mode ()
    (setq truncate-lines nil)
    (setq word-wrap t)))

(use-package gnus-salt
  :defer t

  :custom
  (gnus-pick-display-summary t))

(use-package gnus-search
  :defer t

  :custom
  (gnus-search-use-parsed-queries t))

(use-package gnus-sum
  :defer t

  :general-config
  (:keymaps 'gnus-summary-mode-map
            "M-," #'gnus-catchup-and-next-by-score+
            "DEL" 'gnus-summary-prev-page-or-expand+)
  (:keymaps 'gnus-summary-thread-map
            "N" #'gnus-summary-next-toplevel-thread+
            "P" #'gnus-summary-prev-toplevel-thread+)
  (:keymaps 'gnus-summary-limit-map
            "l" #'gnus-summary-limit-to-same-list+
            "#" #'gnus-summary-limit-to-processable+)
  (:keymaps 'gnus-summary-backend-map
            "a" #'gnus-summary-archive-article+)
  (:keymaps 'gnus-summary-mark-map
            "RET" #'gnus-summary-universal-argument)

  :gfhook
  ('gnus-summary-exit-hook 'gnus-summary-bubble-group)
  ('gnus-summary-mode-hook 'hl-line-mode)

  :custom
  (gnus-ignored-from-addresses
   '("Johann Klähn" "kljohann@gmail\\.com" "@jklaehn\\.de\\'"))

  ;; We only use %z as a marker for updating our custom score display,
  ;; as gnus natively only supports two different mark strings.
  (gnus-score-over-mark ?\s)
  (gnus-score-below-mark ?\s)

  (gnus-summary-default-low-score -49)
  (gnus-summary-default-high-score 49)

  (gnus-build-sparse-threads 'some)
  (gnus-thread-hide-subtree t)
  (gnus-thread-sort-functions
   '(gnus-thread-sort-by-most-recent-number
     gnus-thread-sort-by-total-score))
  (gnus-subthread-sort-functions
   '(gnus-thread-sort-by-number
     gnus-thread-sort-by-total-score))
  (gnus-auto-select-first nil)
  (gnus-auto-select-subject 'first) ; to prevent unfolding of threads

  (gnus-refer-thread-limit 5000)

  (gnus-sum-thread-tree-single-indent   "  ")
  (gnus-sum-thread-tree-false-root      "⨯┐")
  (gnus-sum-thread-tree-root            "· ")
  (gnus-sum-thread-tree-vertical        "│")
  (gnus-sum-thread-tree-leaf-with-other "├╮ ")
  (gnus-sum-thread-tree-single-leaf     "╰╮ ")
  (gnus-sum-thread-tree-indent          " ")
  (gnus-user-date-format-alist
   '(((gnus-seconds-today) .           "       %H:%M")
     ((+ 86400 (gnus-seconds-today)) . "     ◂ %H:%M")
     (604800 .                         "    %a %H:%M") ; up to one week ago
     ((gnus-seconds-year) .            "%d. %b")
     (t .                              "%d. %b %Y")))

  :config
  (let* ((| "%10{|%}")
         (date "%1{%-12,12&user-date;%}")
         (attachment "%9{%u@%}")
         (from "%(%-15,15f %)")
         (size "%4k")
         (score "%2u&score;\ %z")
         (msg-count "%3t")
         (tree "%10{%B%}"))
    (setq gnus-summary-dummy-line-format (concat (make-string 37 ?\ ) "? %S\n"))
    (setq gnus-summary-line-format
          (concat "%U%R" | date | attachment from | msg-count " " size | score " " tree "%*%s\n"))
    (setq gnus-summary-pick-line-format (concat "%-5P " gnus-summary-line-format)))

  (setq gnus-generate-tree-function
        (lambda (&rest args)
          (apply
           (if (my:gnus/prefer-horizontal-p)
               'gnus-generate-vertical-tree
             'gnus-generate-horizontal-tree)
           args)))
  (evil-set-initial-state 'gnus-tree-mode 'emacs)

  (defun gnus-user-format-function-@ (header)
    "Return attachment indicator for mail with given HEADER."
    (cl-assert (and (memq 'Content-Type nnmail-extra-headers)
                    (memq 'Content-Type gnus-extra-headers)))
    (let ((case-fold-search t)
          (ctype
           (alist-get 'Content-Type (mail-header-extra header) "text/plain")))
      (if (string-match "^multipart/mixed" ctype)
          "@"
        " ")))

  (defun gnus-summary-limit-to-processable+ ()
    "Limit the summary buffer to articles that can be processed."
    (interactive)
    (gnus-summary-limit gnus-newsgroup-processable))

  (defun gnus-summary-limit-to-same-list+ ()
    "Limit the summary buffer to articles from the same mailing list as
the current article."
    (interactive)
    (when-let* ((headers (gnus-summary-article-header))
                (list-id (alist-get 'List-ID (mail-header-extra headers))))
      (gnus-summary-limit-to-extra 'List-ID (regexp-quote list-id))))

  (defun gnus-user-format-function-score (header)
    "Return pretty-printed article score for mail with given HEADER."
    (mapconcat
     (lambda (score)
       (cond ((>= score   100) "▲")
             ((<= score  -100) "▼")
             ((>= score    50) "△")
             ((<= score   -50) "▽")
             ((>= score    10) "⌃")
             ((<= score   -10) "⌄")
             ((>  score     0) "⌢")
             ((<  score     0) "⌣")
             (t                " ")))
     (list
      (gnus-thread-total-score (gnus-id-to-thread (mail-header-id header)))
      (gnus-summary-article-score (mail-header-number header)))
     ""))

  (defun my:gnus/summary-position-point ()
    (if (my:frame-small-p)
        (beginning-of-line)
      (gnus-goto-colon)))

  (defalias 'gnus-summary-position-point 'my:gnus/summary-position-point)

  (defun gnus-summary-prev-page-or-expand+ (&optional arg)
    "Show the previous page of the current article or expand the summary buffer.
If given a prefix argument, always expand the summary buffer."
    (interactive "P")
    (cond ((not (and gnus-article-buffer
                     (gnus-get-buffer-window gnus-article-buffer)))
           ;; If article buffer is hidden: jump to parent if thread is collapsed
           ;; else collapse thread.
           (or (gnus-summary-hide-thread)
               (gnus-summary-up-thread 1)))
          ((or arg (gnus-eval-in-buffer-window gnus-article-buffer
                     (pos-visible-in-window-p (point-min))))
           ;; If at start of article: hide article buffer.
           (gnus-summary-expand-window))
          ;; Else scroll up.
          (t (gnus-summary-prev-page))))

  (defun gnus-summary-archive-article+ (&optional n)
    "Move the current article to the Archive newsgroup.
If N is a positive number, move the N next articles.
If N is a negative number, move the N previous articles.
If N is nil and any articles have been marked with the process mark,
move those articles instead."
    (interactive "P")
    (unless (string-match-p "\\`nnimap\\+mail:.*" gnus-newsgroup-name)
      (user-error "The current group does not support archiving"))
    ;; Mark unread articles as read first.
    (dolist (article (gnus-summary-work-articles n))
      (gnus-summary-goto-subject article)
      (when (gnus-unread-mark-p (gnus-summary-article-mark article))
        (gnus-summary-mark-article article gnus-del-mark)))
    (gnus-summary-move-article n "nnimap+mail:Archive"))

  (defun gnus-catchup-and-next-by-score+ (&optional arg)
    "Catchup current article and jump to subject with the highest score.
If given a prefix argument, select the next unread article that has a
score higher than the default score."
    (interactive "P")
    (gnus-summary-mark-article-as-read gnus-catchup-mark)
    (let ((article (if arg
                       (gnus-summary-better-unread-subject)
                     (gnus-summary-best-unread-subject))))
      (if article
          (gnus-summary-show-thread)
        (error "No unread articles"))))

  (defun gnus-summary-next-toplevel-thread+ (n)
    (interactive "p")
    (gnus-summary-top-thread)
    (gnus-summary-hide-thread)
    (gnus-summary-next-thread n))

  (defun gnus-summary-prev-toplevel-thread+ (n)
    (interactive "p")
    (gnus-summary-hide-thread)
    (gnus-summary-next-thread (- n)))

  (define-advice gnus-summary-update-mark (:after (_mark type) custom-score-display)
    (when (eql type 'score)
      (let ((inhibit-read-only t)
            (header (gnus-summary-article-header))
            (forward (cdr (assq type gnus-summary-mark-positions))))
        (save-excursion
          (goto-char (+ forward (line-beginning-position) -2))
          (delete-region (point) (+ (point) 2))
          (insert (gnus-user-format-function-score header)))))))

(use-package gnus-group
  :defer t

  :gfhook
  ('gnus-exit-gnus-hook 'gnus-draft-reminder)
  ('gnus-group-mode-hook '(gnus-topic-mode
                           hl-line-mode))

  :custom
  (gnus-list-groups-with-ticked-articles nil))

(use-package gnus-topic
  :defer t

  :custom
  (gnus-topic-display-empty-topics nil))

(use-package gnus-agent
  :defer t

  ;; :gfhook ('gnus-agent-plugged-hook 'gnus-group-send-queue)

  :custom
  (gnus-agent-prompt-send-queue t)
  (gnus-agent-queue-mail 'if-unplugged))

(use-package gnus-cite
  :defer t

  :custom
  (gnus-cite-hide-absolute 10)
  (gnus-cite-hide-percentage 50)
  (gnus-cited-lines-visible '(2 . 3)))

(use-package gnus-async
  :defer t

  :custom
  (gnus-asynchronous t))

(use-package google-c-style
  :after cc-mode

  :config
  (c-add-style "google" google-c-style)
  (setf (cdr (assq 'other c-default-style)) "google"))

(use-package grep
  :defer t

  :custom
  (grep-use-headings t))

(use-package help
  :defer t

  :custom
  (describe-bindings-outline t))

(use-package hideshow
  :diminish hs-minor-mode
  :ghook ('prog-mode-hook #'hs-minor-mode)

  :custom
  (hs-allow-nesting t)
  (hs-isearch-open t)

  :config
  ;; From https://www.emacswiki.org/emacs/HideShow:
  (defun hs-hide-all-comments ()
    "Hide all top level blocks, if they are comments, displaying only first line.
Move point to the beginning of the line, and run the normal hook
`hs-hide-hook'.  See documentation for `run-hooks'."
    (interactive)
    (hs-life-goes-on
     (save-excursion
       (unless hs-allow-nesting
         (hs-discard-overlays (point-min) (point-max)))
       (goto-char (point-min))
       (let ((spew (make-progress-reporter "Hiding all comment blocks..."
                                           (point-min) (point-max)))
             (re (concat "\\(" hs-c-start-regexp "\\)")))
         (while (re-search-forward re (point-max) t)
           (if (match-beginning 1)
               ;; found a comment, probably
               (let ((c-reg (hs-inside-comment-p)))
                 (when (and c-reg (car c-reg))
                   (if (> (count-lines (car c-reg) (nth 1 c-reg)) 1)
                       (hs-hide-block-at-point t c-reg)
                     (goto-char (nth 1 c-reg))))))
           (progress-reporter-update spew (point)))
         (progress-reporter-done spew)))
     (beginning-of-line)
     (run-hooks 'hs-hide-hook)))

  (general-nmap "zC" 'hs-hide-level)
  (general-nmap "zM" 'hs-hide-all-comments))

(use-package hippie-exp
  :defer t

  :custom
  (hippie-expand-try-functions-list
   '(try-complete-file-name-partially
     try-complete-file-name
     try-expand-all-abbrevs
     try-expand-list
     try-expand-line
     try-expand-dabbrev
     try-expand-dabbrev-visible
     try-expand-dabbrev-all-buffers
     try-expand-dabbrev-from-kill))
  (evil-complete-next-func 'hippie-expand)
  (evil-complete-previous-func #'(lambda (_arg) (hippie-expand t)))

  :general
  ([remap dabbrev-expand] 'hippie-expand))

(use-package hl-todo
  :defer 5
  :general
  (general-imap "C-c C-t" 'hl-todo-insert)

  :custom
  (hl-todo-highlight-punctuation "!,.:")
  (global-hl-todo-mode t)

  :config
  (let ((note-color (cdr (assoc "NOTE" hl-todo-keyword-faces))))
    (setf (alist-get "MARK" hl-todo-keyword-faces nil nil 'equal) note-color)
    (setf (alist-get "Note" hl-todo-keyword-faces nil nil 'equal) note-color))

  ;; Move often-used keywords to the front.
  (let ((keywords '("TODO" "FIXME" "NOTE" "HACK")))
    (mapc (lambda (entry)
            (push entry hl-todo-keyword-faces))
          (mapcar
           (lambda (keyword)
             (prog1 (assoc keyword hl-todo-keyword-faces)
               (setq hl-todo-keyword-faces
                     (assoc-delete-all keyword hl-todo-keyword-faces))))
           (reverse keywords))))

  (define-advice hl-todo-insert
      (:around (fun &rest args) fix-comment-start)
    "Remove spaces from `comment-start' to avoid extra spaces.
This is necessary for `python-mode', for example."
    (let ((comment-start (string-trim comment-start)))
      (apply fun args)))

  (define-advice hl-todo-insert (:after (_keyword) add-user-login-name)
    "Add `user-login-name' after TODO keyword."
    (skip-chars-backward " \t")
    (delete-trailing-whitespace (point) (pos-eol))
    (if (fboundp 'yas-expand-snippet)
        (yas-expand-snippet
         (concat "${1:$(when (yas-text) \"(\")}"
                 "${1:" user-login-name "}"
                 "${1:$(when (yas-text) \")\")}"
                 ": $0"))
      (insert "(" user-login-name "): "))))

(use-package holidays
  :defer t

  :custom
  (holiday-bahai-holidays nil)
  (holiday-hebrew-holidays nil)
  (holiday-islamic-holidays nil)
  (holiday-oriental-holidays nil)
  (holiday-solar-holidays nil)

  (holiday-general-holidays
   '((holiday-fixed 1 1 "Neujahr")
     (holiday-fixed 5 1 "1. Mai")
     (holiday-fixed 10 3 "Tag der Deutschen Einheit")))

  (holiday-christian-holidays
   '((holiday-advent  0 "1. Advent")
     (holiday-advent  7 "2. Advent")
     (holiday-advent 14 "3. Advent")
     (holiday-advent 21 "4. Advent")
     (holiday-fixed 12 25 "1. Weihnachtstag")
     (holiday-fixed 12 26 "2. Weihnachtstag")
     (holiday-fixed 1 6 "Heilige Drei Könige")
     (holiday-easter-etc -48 "Rosenmontag")
     (holiday-easter-etc  -3 "Gründonnerstag")
     (holiday-easter-etc  -2 "Karfreitag")
     (holiday-easter-etc   0 "Ostersonntag")
     (holiday-easter-etc  +1 "Ostermontag")
     (holiday-easter-etc +39 "Christi Himmelfahrt")
     (holiday-easter-etc +49 "Pfingstsonntag")
     (holiday-easter-etc +50 "Pfingstmontag")
     (holiday-easter-etc +60 "Fronleichnam")
     (holiday-fixed 8 15 "Mariä Himmelfahrt")
     (holiday-fixed 11 1 "Allerheiligen")
     (holiday-float 11 3 1 "Buß- und Bettag" 16)
     (holiday-float 11 0 1 "Totensonntag" 20))))

(use-package ibuffer
  :defer t
  :general
  (my:general/leader "b" 'ibuffer)
  (:keymaps 'ctl-x-map "C-b" 'ibuffer))

(use-package icomplete
  :defer t

  :general-config
  (:keymaps 'icomplete-minibuffer-map
            "RET" 'icomplete-force-complete-and-exit
            "M-RET" 'exit-minibuffer
            "M-j" 'icomplete-fido-exit
            "DEL" 'icomplete-fido-backward-updir
            "<down>" 'icomplete-forward-completions
            "C-n" 'icomplete-forward-completions
            "<up>" 'icomplete-backward-completions
            "C-p" 'icomplete-backward-completions)
  :gfhook ('icomplete-minibuffer-setup-hook #'visual-line-mode)

  :custom
  (icomplete-hide-common-prefix nil)
  (icomplete-tidy-shadowed-file-names t)
  (icomplete-show-matches-on-no-input t)
  (icomplete-mode nil))

(use-package isearch
  :defer t

  :general
  ;; `search-map' (M-s)
  ("M-s M-<" 'isearch-beginning-of-buffer)
  ("M-s M->" 'isearch-end-of-buffer)

  :custom
  (search-whitespace-regexp ".*?")
  (isearch-lazy-count t)
  (isearch-yank-on-move 'shift))

(use-package ispell
  :defer t

  :custom
  (ispell-silently-savep t)
  (ispell-program-name (or (executable-find "hunspell")
                           (executable-find "enchant-2")
                           (executable-find "aspell")
                           (executable-find "ispell")
                           "ispell")))

(use-package jit-spell
  :defer t

  :ghook
  ('text-mode-hook #'jit-spell-mode)
  ('prog-mode-hook #'jit-spell-mode)

  :general-config
  (:keymaps 'jit-spell-mode-map
            "C-c DEL" 'jit-spell-correct-word))

(use-package keyfreq
  :demand t

  :custom
  (keyfreq-mode t)
  (keyfreq-autosave-mode t))

(use-package kmacro
  :defer t

  :general
  (my:general/leader "k" 'kmacro-keymap))

(use-package kind-icon
  :defer t
  :after corfu

  :custom
  (kind-icon-default-face 'corfu-default)

  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package latex
  :defer t

  :mode ((".[bcl]bx\\'" . LaTeX-mode))

  :custom
  (TeX-parse-self t)
  (TeX-auto-save t)
  (TeX-save-query nil)

  (TeX-force-default-mode t)
  (TeX-default-mode 'latex-mode)
  (TeX-insert-braces nil)
  (TeX-electric-sub-and-superscript t)
  (TeX-electric-math '("\\(" . "\\)"))
  (TeX-arg-input-file-search nil)
  (LaTeX-includegraphics-read-file 'LaTeX-includegraphics-read-file-relative)

  (TeX-error-overview-open-after-TeX-run t)

  (font-latex-fontify-script nil)

  (TeX-view-program-selection '((output-pdf "PDF Tools")))
  (TeX-source-correlate-start-server t)

  (TeX-master nil)
  (TeX-PDF-mode t)

  (LaTeX-babel-hyphen nil)
  (LaTeX-csquotes-close-quote "}")
  (LaTeX-csquotes-open-quote "\\enquote{")

  :config
  (dolist (suffix '("\\.acn" "\\.acr" "\\.fff"
                    "\\.glg" "\\.glo" "\\.gls"
                    "\\.glsdefs"
                    "\\.glg-abr" "\\.glo-abr" "\\.gls-abr"
                    "\\.slg" "\\.slo" "\\.sls"))
    (add-to-list 'LaTeX-clean-intermediate-suffixes suffix 'append))

  (add-hook 'TeX-after-compilation-finished-functions
            'TeX-revert-document-buffer)

  (defun hooks:latex-mode-setup ()
    (reftex-mode 1)
    (TeX-fold-mode 1)
    (LaTeX-math-mode 1)
    (variable-pitch-mode 1)
    (LaTeX-preview-setup)
    (TeX-source-correlate-mode 1))
  (add-hook 'LaTeX-mode-hook #'hooks:latex-mode-setup))

(use-package link-hint
  :defer t

  :general
  (my:general/leader
    "j" 'link-hint-open-link
    "J" 'link-hint-open-multiple-links))

(use-package llvm-mode
  :commands llvm-mode
  :defer t
  :mode ("\\.ll\\'" . llvm-mode))

(use-package magit
  :defer t
  :functions (magit-add-section-hook)
  :hook ((magit-process-mode magit-revision-mode) . goto-address-mode)

  :general
  (my:general/leader
    "m" 'magit-status
    "g m" 'magit-status-here
    "M" 'magit-file-dispatch)
  :general-config
  (:keymaps 'magit-diff-mode-map
            "S-RET" 'magit-diff-visit-file-other-window)
  (:keymaps 'magit-mode-map
            "\C-c l" 'magit-toggle-buffer-lock)
  (:keymaps 'magit-revision-mode-map
            "DEL" 'magit-scroll-down+)
  (:keymaps 'magit-section-mode-map
            "C-c M-w" 'magit-copy-section-value
            "{" 'magit-section-backward
            "}" 'magit-section-forward
            "(" 'magit-section-backward-sibling
            ")" 'magit-section-forward-sibling)

  :custom
  (magit-save-repository-buffers 'dontask)
  (magit-section-visibility-indicator nil)
  (magit-section-show-child-count t)
  (magit-diff-refine-hunk t)
  (magit-display-buffer-function 'magit-display-buffer-fullframe-status-v1)
  (magit-bury-buffer-function 'magit-restore-window-configuration)
  (magit-section-initial-visibility-alist '((stashes . hide) (untracked . hide)))
  (magit-status-initial-section '(((unstaged) (status)) ((staged) (status)) 1))

  (magit-commit-squash-confirm nil)
  (magit-commit-extend-override-date nil)
  (magit-commit-reword-override-date nil)

  (magit-log-revision-headers-format "%+b")

  (magit-diff-refine-hunk 'all)

  (magit-branch-adjust-remote-upstream-alist '(("master" . "/")))
  (magit-prefer-push-default t)
  (magit-pull-or-fetch t)

  ;; TODO: Increase `core.abbrev' during formatting? `%<(12,trunc)' appends `..' :(
  (magit-pop-revision-stack-format '("%h (“%s”, %cs)" "%cs: %H (%aN)" nil))

  :config
  (add-hook 'magit-section-movement-hook #'magit-status-maybe-update-revision-buffer)
  (add-hook 'magit-section-movement-hook #'magit-status-maybe-update-blob-buffer)
  (add-hook 'magit-section-movement-hook #'magit-status-maybe-update-stash-buffer)
  (add-hook 'magit-section-movement-hook #'magit-stashes-maybe-update-stash-buffer)
  (add-hook 'magit-section-movement-hook #'magit-log-maybe-update-blob-buffer)

  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-worktrees
                          'magit-insert-stashes
                          'append)

  (defun magit-scroll-down+ (&optional arg)
    "Scroll text down ARG lines then bury buffer (close window) when reaching the top."
    (interactive "^P")
    (if (pos-visible-in-window-p (point-min))
        (magit-mode-bury-buffer)
      (scroll-down arg)))

  (defun magit-diff-show-or-scroll-down+ ()
    "Like `magit-diff-show-or-scroll-down', but bury buffer (close window) when reaching the top."
    (interactive)
    (magit-diff-show-or-scroll #'magit-scroll-down+))
  (advice-add 'magit-diff-show-or-scroll-down :override #'magit-diff-show-or-scroll-down+)

  (defun my:git/branch-name-from-subject (&optional rev)
    "Derive branch name from subject of REV and prefix of current branch."
    (let ((branch (magit-get-current-branch))
          (branch-prefix-rx
           (rx (+ letter) "/"
               (? (+ letter) "-" (+ digit) "-")))
          (subject (string-replace "_" "-" (downcase (magit-rev-format "%f" rev)))))
      (concat
       (and branch (string-match branch-prefix-rx branch)
            (match-string 0 branch))
       subject)))

  ;; https://github.com/magit/magit/issues/460#issuecomment-837449105
  (define-advice magit-process-environment
      (:filter-return (env) use-separate-git-dir-in-home)
    "When the closest dominating (.git|.cfg) is ~/.cfg, use that as the git dir."
    ;; An alternative is to have a `.git' file containing `gitdir: /path/...'
    ;; and set `core.worktree' to point to `$HOME', as set up by
    ;; `clone --separate-git-dir'.  But that would also have an effect on plain
    ;; `git' calls in a terminal.
    (when-let* ((work-tree
                 (locate-dominating-file
                  default-directory
                  (lambda (dir)
                    (or (file-exists-p (expand-file-name ".cfg" dir))
                        (file-exists-p (expand-file-name ".git" dir))))))
                (work-tree (expand-file-name work-tree))
                (cfg-directory (expand-file-name ".cfg" work-tree)))
      (and (f-same? work-tree "~/")
           (file-directory-p cfg-directory)
           (setq env
                 (nconc
                  `(,(format "GIT_WORK_TREE=%s" work-tree)
                    ,(format "GIT_DIR=%s" cfg-directory))
                  env))))
    env)

  (define-advice magit-branch-rename
      (:before (&rest _args) suggest-name-based-on-subject)
    "Use subject of commit under point as default value for new branch name."
    (interactive
     (let* ((branch (magit-read-local-branch "Rename branch"))
            (rev (or (magit-branch-or-commit-at-point)
                     branch (magit-get-current-branch)))
            (default (if current-prefix-arg
                         branch
                       (my:git/branch-name-from-subject rev))))
       (list branch
             (magit-read-string-ns (format "Rename branch '%s' to" branch)
                                   nil 'magit-revision-history default)
             current-prefix-arg))))

  (defun magit-cherry-isolate+ (commits branch start-point &optional args)
    "Copy COMMITS from the current branch onto a new BRANCH.
Suggest a branch name based on the newest commit and stay on that branch.
If a conflict occurs, then you have to fix that and finish the
process manually."
    (interactive
     (magit--cherry-move-read-args "isolate" t
       (lambda (commits)
         ;; Overwrite magit-read-string-ns to provide branch name suggestion.
         (cl-letf* ((read-string-fn (symbol-function 'magit-read-string-ns))
                    (suggested-branch-name
                     (my:git/branch-name-from-subject (car (last commits))))
                    ((symbol-function 'magit-read-string-ns)
                     (lambda (prompt &optional initial-input history
                                     default-value inherit-input-method)
                       (funcall read-string-fn prompt
                                (if (cl-equalp prompt "Name for new branch")
                                    suggested-branch-name
                                  initial-input)
                                history
                                default-value
                                inherit-input-method))))
           (magit-branch-read-args
            (format "Create branch from %s cherries" (length commits))
            (magit-get-upstream-branch))))))
    (magit--cherry-move commits nil branch args start-point))

  (transient-append-suffix 'magit-cherry-pick "s"
    '("i" "Isolate" magit-cherry-isolate+))

  (defun magit-commit-create-lazy+ (&optional args)
    (interactive (list (magit-commit-arguments)))
    (magit-commit-create (append args `("-m" ,(concat "Track changes on " (system-name))))))

  (transient-append-suffix 'magit-commit "c"
    '("C" "Commit (lazy)" magit-commit-create-lazy+)))

(use-package eldoc-diffstat
  :defer t
  :functions eldoc-diffstat-setup

  :ghook
  ('git-rebase-mode-hook #'eldoc-diffstat-setup)
  ('log-view-mode-hook #'eldoc-diffstat-setup)
  ('magit-log-mode-hook #'eldoc-diffstat-setup)
  ('magit-status-mode-hook #'eldoc-diffstat-setup)
  ('vc-annotate-mode-hook #'eldoc-diffstat-setup)

  :config
  (with-eval-after-load 'eldoc
    (eldoc-add-command 'magit-next-line 'magit-previous-line
                       'magit-section-forward 'magit-section-backward
                       'magit-section-forward-sibling
                       'magit-section-backward-sibling)))

(use-package magit-blame
  :no-require t

  :gfhook
  ('magit-blame-read-only-mode-on-hook #'evil-emacs-state)
  ('magit-blame-read-only-mode-off-hook #'evil-normal-state))

(use-package magit-tbdiff
  :defer t

  :config
  (defun magit-tbdiff-branch-with-pushremote+ (branch &optional args)
    "Compare BRANCH to its push-remote, using its upstream as the base."
    (interactive
     (list (magit-read-local-branch "Branch")
           (transient-args 'magit-tbdiff)))
    (let ((target (magit-get-push-branch branch t))
          (upstream (magit-get-upstream-branch branch)))
      (unless target
        (user-error "No push-remote configured for %s" branch))
      (unless upstream
        (user-error "No upstream configured for %s" branch))
      (magit-tbdiff-revs-with-base target branch upstream args)))

  (transient-append-suffix 'magit-tbdiff "r"
    '("p" "Compare local branch with push-remote" magit-tbdiff-branch-with-pushremote+)))

(use-package mail-source
  :defer t

  :custom
  (mail-sources nil))

(use-package marginalia
  :demand t

  :custom
  (marginalia-mode t))

(use-package message
  :defer t

  :gfhook
  ;; Add a score to both articles that appear in a thread “below” my
  ;; article and to articles that directly follow up my article (×2).
  ('message-sent-hook '(gnus-score-followup-thread
                        gnus-score-followup-article))
  ('message-mode-hook '(footnote-mode
                        flyspell-mode
                        turn-on-auto-fill))

  :custom
  (message-directory (expand-file-name "~/sync/gnus/mail/"))
  (message-auto-save-directory (expand-file-name "~/sync/gnus/mail/drafts/" message-directory))
  (message-confirm-send t)
  (message-send-mail-function 'message-send-mail-with-sendmail)
  (message-sendmail-envelope-from 'header)
  (message-alternative-emails (rx "@jklaehn.de" eos))
  (message-wide-reply-confirm-recipients t)
  (message-citation-line-function 'message-insert-formatted-citation-line)
  (message-citation-line-format "On %a, %b %d, %Y at %H:%M %z, %N wrote:")
  (message-subject-re-regexp
   (rx-let ((sp (in " \t"))
            (icase (s) (eval
                        (list 'regexp
                              (cl-loop for c being the elements of s
                                       with u with l
                                       do (setq u (upcase (char-to-string c))
                                                l (downcase (char-to-string c)))
                                       if (string-equal u l) concat l
                                       else concat "[" and
                                       concat l and concat u and
                                       concat "]" end)))))
     (rx bol (* sp)
         (* (| (: (icase "antw") (? "."))
               (icase "aw")
               (icase "fwd?")
               (icase "re"))
            (* "[" (* num) "]")
            ":" (* sp))
         (* sp))))

  :config
  (define-advice message-insert-formatted-citation-line
      (:around (orig-fun &rest args) use-c-locale)
    (let ((system-time-locale "C"))
      (apply orig-fun args))))

(defun my:minibuffer/use-builtin-completion-in-region ()
  "Always use the built-in `completion-in-region-function' in minibuffers."
  (unless (equal "Eval: " (minibuffer-prompt))
    (setq-local completion-in-region-function #'completion--in-region)))

(use-package minibuffer
  :defer t

  :general-config
  ;; TODO: revisit?
  (:keymaps 'minibuffer-local-completion-map
            "RET" #'minibuffer-force-complete-and-exit
            "M-RET" #'exit-minibuffer
            "M-TAB" #'minibuffer-force-complete
            "<backtab>" #'minibuffer-force-complete)
  (:keymaps 'minibuffer-local-filename-completion-map
            "RET" #'minibuffer-force-complete-and-exit)

  :ghook ('minibuffer-setup-hook #'my:minibuffer/use-builtin-completion-in-region)

  :custom
  (completion-category-defaults nil)
  ;; Tramp hostname completion only works if `basic' comes first.
  (completion-category-overrides '((file (styles . (basic partial-completion)))))
  (completion-cycle-threshold nil)
  (completion-show-help nil)

  (completion-ignore-case t)
  (read-buffer-completion-ignore-case t)
  (read-file-name-completion-ignore-case t)

  (enable-recursive-minibuffers t)
  (minibuffer-visible-completions t)
  (minibuffer-beginning-of-buffer-movement t)
  (read-answer-short t)
  (resize-mini-windows t)

  (file-name-shadow-mode t)
  (minibuffer-depth-indicate-mode t)
  (minibuffer-electric-default-mode t)
  (minibuffer-prompt-properties
   '( read-only t
      face minibuffer-prompt
      cursor-intangible t))

  :config
  (define-advice eglot--dumb-tryc (:around (origfun pat table pred point) withhold-common-prefix)
    "Do not insert the longest common prefix for completion items with text edits."
    (let ((result (funcall origfun pat table pred point)))
      (if (consp result)
          (cl-destructuring-bind (new-pat . new-point) result
            (if (plist-get (get-text-property 0 'eglot--lsp-item new-pat) :textEdit)
                (cons pat point)
              result))
        result)))

  (define-advice minibuffer-force-complete-and-exit
      (:around (fun &rest args) inhibit-message)
    "Inhibit messages when exiting minibuffer after a completion."
    (let ((minibuffer-message-clear-timeout 0))
      (apply fun args))))

(use-package minions
  :defer t

  :custom
  (minions-mode-line-delimiters '("" . ""))
  (minions-prominent-modes '(envrc-mode flymake-mode))
  (minions-mode t))

(use-package mode-scratch
  :commands mode-scratch
  :defer t)

(use-package mouse
  :defer t

  :custom
  (mouse-1-click-follows-link nil)
  (mouse-drag-copy-region nil)
  (mouse-yank-at-point t)
  (context-menu-mode t))

(use-package mwheel
  :defer t

  :custom
  (mouse-wheel-scroll-amount
   '( 1
      ((shift) . hscroll)
      ((meta) . nil)
      ((control) . text-scale)))
  (mouse-wheel-progressive-speed t))

(use-package nasm-mode
  :defer t)

(use-package nav-flash
  :demand t
  :hook (evil-normal-state-entry . nav-flash-show)

  :config
  ;; Inspired by
  ;; https://with-emacs.com/posts/ui-hacks/keep-scrollin-scrollin-scrollin/
  (defun my:nav-flash/flash-before-scroll (pos)
    ""
    (nav-flash-show pos nil 'secondary-selection 0.3)
    ;; (remove-hook 'pre-command-hook 'compilation-goto-locus-delete-o)
    (sit-for 0.05 t))

  (define-advice scroll-down (:before (&rest _args) nav-flash)
    (my:nav-flash/flash-before-scroll
     (save-excursion
       (goto-char (window-start))
       (forward-line (1- next-screen-context-lines))
       (point))))

  (define-advice scroll-up (:before (&rest _args) nav-flash)
    (my:nav-flash/flash-before-scroll
     (save-excursion
       (goto-char (window-end))
       (forward-line (- next-screen-context-lines))
       (point))))

  :custom
  (nav-flash-delay .2))

(use-package nsm
  :defer t
  :custom
  (nsm-save-host-names t)
  (network-security-level 'high))

(use-package objdump
  :defer t
  :commands objdump-mode
  :magic ("ELF" . objdump-mode))

(use-package olivetti
  :defer t

  :custom
  (olivetti-body-width 80)

  :ghook ('LaTeX-mode-hook #'olivetti-mode))

(use-package orderless
  :demand t

  :general-config
  ( :keymaps 'minibuffer-local-completion-map
    "SPC" nil)

  :custom
  (completion-styles '(orderless basic))
  (gnus-completion-styles '(orderless basic))
  (orderless-expand-substring 'substring)
  (orderless-component-separator #'orderless-escapable-split-on-space)
  (orderless-style-dispatchers `(,#'orderless-affix-dispatch
                                 ,#'orderless-kwd-dispatch))
  (orderless-matching-styles '(orderless-regexp
                               orderless-prefixes)))

(use-package outline
  :defer t

  :ghook ('prog-mode-hook #'outline-minor-mode))

(use-package package
  :defer t

  :config
  (define-advice package-menu--mark-upgrades-1 (:after () unmark-deletions)
    "Unmark all packages marked for deletion.

Upgrading should be a two-step process of installing the new version and then explicitly
deleting the old version using `~ x'."
    (interactive)
    (widen)
    (save-excursion
      (goto-char (point-min))
      (while (not (eobp))
        (when (eql (char-after) ?\D)
          (tabulated-list-put-tag " "))
        (forward-line)))))

(use-package paren
  :demand t

  :custom
  (blink-matching-paren nil)
  (show-paren-style 'parenthesis)
  (show-paren-when-point-inside-paren t)
  (show-paren-when-point-in-periphery t)

  :custom
  (show-paren-mode t))

(use-package paren-face
  :demand t

  :custom
  (global-paren-face-mode 1))

(use-package parse-time
  :defer t

  :config
  (setq parse-time-weekdays
        '(("mo" . 1) ("montag" . 1) ("mon" . 1) ("monday" . 1)
          ("di" . 2) ("dienstag" . 2) ("tue" . 2) ("tuesday" . 2)
          ("mi" . 3) ("mittwoch" . 3) ("wed" . 3) ("wednesday" . 3)
          ("do" . 4) ("donnerstag" . 4) ("thu" . 4) ("thursday" . 4)
          ("fr" . 5) ("freitag" . 5) ("fri" . 5) ("friday" . 5)
          ("sa" . 6) ("samstag" . 6) ("sat" . 6) ("saturday" . 6)
          ("so" . 0) ("sonntag" . 0) ("sun" . 0) ("sunday" . 0)))
  (setq parse-time-months
        '(("jan" . 1) ("januar" . 1) ("january" . 1)
          ("feb" . 2) ("februar" . 2) ("february" . 2)
          ("mär" . 3) ("märz" . 3) ("mar" . 3) ("march" . 3)
          ("apr" . 4) ("april" . 4)
          ("mai" . 5) ("may" . 5)
          ("jun" . 6) ("juni" . 6) ("june" . 6)
          ("jul" . 7) ("juli" . 7) ("july" . 7)
          ("aug" . 8) ("august" . 8)
          ("sep" . 9) ("september" . 9)
          ("okt" . 10) ("oktober" . 10) ("oct" . 10) ("october" . 10)
          ("nov" . 11) ("november" . 11)
          ("dez" . 12) ("dezember" . 12) ("dec" . 12) ("december" . 12))))

(use-package pdf-view
  :defer t

  :mode ("\\.pdf\\'" . pdf-view-mode)
  :magic ("%PDF" . pdf-view-mode)
  :hook (pdf-view-mode . pdf-tools-enable-minor-modes)

  :general-config
  (:keymaps 'pdf-view-mode-map
            "<mouse-4>" 'pdf-view-previous-line-or-previous-page
            "<mouse-5>" 'pdf-view-next-line-or-next-page)

  :custom
  ;; use midnight-mode as preview of grayscale printing
  (pdf-view-midnight-colors nil))

(use-package pdf-cache
  :defer t

  :init (setq image-cache-eviction-delay 10)
  :custom (pdf-cache-image-limit 5))

(use-package pdf-occur
  :defer t

  :custom
  (pdf-occur-prefer-string-search t))

(use-package pdf-outline
  :defer t

  :custom
  (pdf-outline-imenu-use-flat-menus t))

(use-package pdf-annot
  :defer t
  :defines pdf-annot-minor-mode-map-prefix

  :init
  (setq pdf-annot-minor-mode-map-prefix "t")

  :config
  (setf (cdr (assq t pdf-annot-default-annotation-properties))
        `((label . ,user-full-name)
          (popup-is-open . nil)))
  (setf (cdr (assq 'text pdf-annot-default-annotation-properties))
        `((icon . "Note")
          (color . "khaki")))
  (define-advice pdf-annot-add-annotation (:around (orig-fun type &rest args) active-if-text)
    (let ((pdf-annot-activate-created-annotations (eql type 'text)))
      (apply orig-fun type args))))

(use-package pikchr-mode
  :defer t

  :config
  (with-eval-after-load 'evil-core
    (add-to-list 'evil-buffer-regexps
                 '("\\` \\*pikchr preview\\*" . emacs))))

(use-package preview ; preview-latex
  :defer t

  :config
  (add-to-list 'preview-default-preamble "\\PreviewEnvironment{tikzpicture}" t)
  (add-to-list 'preview-default-option-list "xetex"))

(use-package projectile
  :defer t

  :general
  (my:general/leader "p" '(:keymap projectile-command-map))
  :general-config
  (:keymaps 'projectile-command-map [escape] 'projectile-project-buffers-other-buffer)

  :custom
  (projectile-completion-system 'default)
  (projectile-switch-project-action #'projectile-commander)
  (projectile-project-name-function 'my:projectile/project-name)
  (projectile-mode-line-prefix " ⚑")
  (projectile-mode-line-function #'ignore)
  (projectile-dynamic-mode-line nil)
  (projectile-use-git-grep t)
  (projectile-find-dir-includes-top-level t)
  (projectile-run-use-comint-mode t)
  (projectile-mode t)

  :config
  (defun my:projectile/project-name (project-root)
    (abbreviate-file-name (directory-file-name project-root)))

  (let ((pofa projectile-other-file-alist)
        (extensions '("h" "hh" "hpp" "hxx")))
    (dolist (ext extensions)
      (push "inl" (alist-get ext pofa nil nil 'string-equal))
      (push ext (alist-get "inl" pofa nil nil 'string-equal)))))

(use-package python
  :defer t

  :custom
  (python-shell-interpreter "ipython")
  (python-shell-interpreter-args "-i --simple-prompt"))

(use-package recentf
  :no-require t

  :custom
  (recentf-filename-handlers nil)
  (recentf-max-saved-items 1000)
  (recentf-max-menu-items 500)
  (recentf-auto-cleanup (* 5 60))
  (recentf-mode t))

(use-package reftex
  :defer t

  :general-config
  (:keymaps 'reftex-mode-map
            ;; make binding for citation `C-c (' match reference `C-c [':
            "C-c (" #'reftex-reference
            "C-c )" #'reftex-label)
  :hook ((LaTeX-mode . turn-on-reftex)
         (latex-mode . turn-on-reftex))

  :custom
  (reftex-default-bibliography '("~/read/read.bib"))
  (reftex-cite-prompt-optional-args 'maybe)
  (reftex-plug-into-AUCTeX t)
  (reftex-use-external-file-finders t)
  (reftex-save-parse-info t)
  (reftex-cite-format 'biblatex)
  (reftex-ref-style-default-list '("Default" "Cleveref"))

  :config
  ;; I may want to manually tag and label a single line in align*.
  (add-to-list
   'reftex-label-alist
   '("align*" ?e nil nil eqnarray-like)))

(use-package repeat
  :no-require t

  :custom
  (repeat-exit-timeout 5)
  (repeat-keep-prefix nil)
  (repeat-mode t))

(use-package replace
  :defer t

  :gfhook ('occur-mode-hook #'hl-line-mode)

  :general-config
  (:keymaps 'occur-mode-map
            "[q" 'previous-error
            "]q" 'next-error))

(use-package savehist
  :no-require t

  :custom
  (savehist-mode t)
  (savehist-autosave-interval (* 15 60))
  (savehist-ignored-variables
   '(bookmark-history
     consult--buffer-history
     minibuffer-history))

  :defines savehist-additional-variables
  :config
  (add-to-list 'savehist-additional-variables 'search-ring)
  (add-to-list 'savehist-additional-variables 'regexp-search-ring)
  (add-to-list 'savehist-additional-variables 'evil-ex-history)
  (add-to-list 'savehist-additional-variables 'evil-ex-search-history))

(use-package saveplace
  :no-require t

  :custom
  (save-place-mode t))

(use-package score-mode
  :defer t

  :mode
  ("\\.ADAPT\\'" . gnus-score-mode)
  ("\\.SCORE\\'" . gnus-score-mode))

(use-package sendmail
  :defer t

  :custom
  (send-mail-function 'sendmail-send-it)
  (sendmail-program (or (executable-find "msmtp") "msmtp"))
  (mail-specify-envelope-from t)
  (mail-envelope-from 'header))

(use-package select
  :no-require t

  :custom
  (select-enable-primary t))

(use-package shr
  :defer t

  :custom
  (shr-cookie-policy nil)
  (shr-discard-aria-hidden t))

(use-package simple
  :no-require t

  :general
  ("M-l" 'cycle-spacing)

  :custom
  (save-interprogram-paste-before-kill t)
  (kill-do-not-save-duplicates t)
  (set-mark-command-repeat-pop t)
  (mark-ring-max 100)
  (global-mark-ring-max 30)
  (what-cursor-show-names t)
  (next-error-message-highlight t)

  (indent-tabs-mode nil)

  (line-number-mode t)
  (column-number-mode t)
  (transient-mark-mode t))

(use-package smerge-mode
  :defer t

  :general
  (my:general/leader "s" '(:keymap smerge-basic-map))
  :general-config
  (general-nmap :keymaps 'smerge-mode-map
    "] c" 'smerge-next
    "[ c" 'smerge-prev)
  (:keymaps 'smerge-basic-map
    "s" 'smerge-transient+)

  :config
  (transient-define-prefix smerge-transient+ ()
    "Resolve diff3 conflicts."
    [["Conflict"
      ("n" "Next" smerge-next :transient t)
      ("p" "Prev" smerge-prev :transient t)
      ("R" "Refine" smerge-refine :transient t)
      ("C" "Combine with next" smerge-combine-with-next :transient t)]
     ["Pick"
      ("a" "Keep all" smerge-keep-all :transient t)
      ("b" "Revert to base" smerge-keep-base :transient t)
      ("u" "Keep upper" smerge-keep-upper :transient t)
      ("l" "Keep lower" smerge-keep-lower :transient t)]
     ["Diff"
      ("E" "Ediff" smerge-ediff)
      ("<" "Diff base/upper" smerge-diff-base-upper)
      (">" "Diff base/lower" smerge-diff-base-lower)
      ("=" "Diff upper/lower" smerge-diff-base-lower)]
     ["Exit"
      ("m" "Magit" magit-status-here)
      ("q" "Quit" transient-quit-all)
      ("DEL" "Bury buffer" bury-buffer)]]
    (interactive)
    (smerge-mode)
    (transient-setup 'smerge-transient+)))

(use-package smiley
  :defer t

  :custom
  (smiley-style 'emoji))

(use-package smtpmail
  :defer t

  :custom
  (smtpmail-smtp-server "smtp.fastmail.com")
  (smtpmail-smtp-service 465)
  (smtpmail-stream-type 'ssl)
  (smtpmail-servers-requiring-authorization (rx "smtp.fastmail.com")))

(use-package solar
  :defer t

  :custom
  (calendar-latitude 49.47)
  (calendar-longitude 8.47)
  (calendar-location-name "Mannheim")
  (calendar-time-display-form
   '(24-hours ":" minutes
              (if time-zone " (") time-zone (if time-zone ")"))))

(use-package tablegen-mode
  :commands tablegen-mode
  :defer t
  :mode ("\\.td\\'" . tablegen-mode))

(use-package tex-site
  :demand t)

(use-package text-mode
  :no-require t

  :custom
  (text-mode-ispell-word-completion nil))

(use-package time
  :defer t

  :custom
  (display-time-24hr-format t))

(use-package tmm
  :defer t

  :custom
  (tmm-completion-prompt nil)
  (tmm-mid-prompt " ==> "))

(use-package tool-bar
  :defer t

  :gfhook ('tool-bar-mode-hook #'modifier-bar-mode)

  :custom
  (tool-bar-position 'bottom))

(use-package touch-screen
  :defer t

  :custom
  (touch-screen-word-select t)
  (touch-screen-preview-select t))

(use-package transient
  :defer t

  :custom
  (transient-semantic-coloring t))

(use-package transpose-frame
  :defer t

  :general
  (:keymaps 'evil-window-map
            "DEL" 'transpose-frame
            "*" 'rotate-frame
            "{" 'rotate-frame-anticlockwise
            "}" 'rotate-frame-clockwise
            "~" 'flip-frame
            ":" 'flop-frame))

(use-package treesit
  :if (treesit-available-p)
  :custom
  (treesit-language-source-alist
   '((c . ("https://github.com/tree-sitter/tree-sitter-c"
           "f4c21152f1952a99f4744e8c41d3ffb8038ae78c"))
     (cmake . ("https://github.com/uyha/tree-sitter-cmake"
               "69d7a8b0f7493b0dbb07d54e8fea96c5421e8a71"))
     (cpp . ("https://github.com/tree-sitter/tree-sitter-cpp"
             "30d2fa385735378388a55917e2910965fce19748"))
     (doxygen . ("https://github.com/tree-sitter-grammars/tree-sitter-doxygen"
                 "ccd998f378c3f9345ea4eeb223f56d7b84d16687"))
     (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript"
                    "b6f0624c1447bc209830b195999b78a56b10a579"))
     (json . ("https://github.com/tree-sitter/tree-sitter-json"
              "8bfdb43f47ad805bb1ce093203cfcbaa8ed2c571"))
     (lua . ("https://github.com/tree-sitter-grammars/tree-sitter-lua"
             "34e60e7f45fc313463c68090d88d742a55d1bd7a"))
     (rust . ("https://github.com/tree-sitter/tree-sitter-rust"
              "6b7d1fc73ded57f73b1619bcf4371618212208b1"))
     (tsx . ("https://github.com/tree-sitter/tree-sitter-typescript"
             "73c4447796b251295b498227bad028d88dc1918b"
             "tsx/src"))
     (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript"
                    "73c4447796b251295b498227bad028d88dc1918b"
                    "typescript/src"))))
  ;; This is over the top, but adjusted for in the theme by ignoring
  ;; certain faces.
  (treesit-font-lock-level 4)

  :config
  (define-advice treesit--git-clone-repo
      (:override (url revision workdir) checkout-revision)
    "Clone full repo and checkout the specified REVISION."
    (message "Cloning %s (rev: %s)" url revision)
    (apply #'treesit--call-process-signal
     "git" nil t nil "clone" url  "--quiet" workdir (unless revision '("--depth" "1")))
    (if revision
        (treesit--call-process-signal
         "git" nil t nil "-C" workdir "switch" "--detach" revision "--quiet"))))

(use-package rust-ts-mode
  :if (treesit-ready-p 'rust 'quiet)
  :defer t

  :mode ("\\.rs\\'" . rust-ts-mode)
  :gfhook ('rust-ts-mode-hook 'eglot-ensure)

  :config
  (with-eval-after-load 'compile
    ;; https://github.com/rust-lang/rust/blob/master/compiler/rustc_errors/src/emitter.rs
    (rx-let ((file (group-n 1 (+ nonl)))
             (line (group-n 2 (+ digit)))
             (col (group-n 3 (+ digit)))
             (loc (group-n 6 (: file ":" line ":" col)))
             (type (| "error" (group-n 4 "warning") (group-n 5 "note")))
             (ansi (| (regexp ansi-color-control-seq-regexp)
                      (regexp ansi-osc-control-seq-regexp)))
             (ansi-or-ws (* (| ansi blank)))
             ;; The first annotated source line uses `-->', we match it together
             ;; with the diagnostic message.
             (diag (: bol (* ansi) type (+? anything)
                      bol ansi-or-ws "-->" ansi-or-ws loc))
             ;; Subsequent annotations use `:::'.
             (secondary (: bol ansi-or-ws ":::" ansi-or-ws loc))
             ;; Line numbers in code snippets.
             (snippet-line (: bol ansi-or-ws line ansi-or-ws "|"))
             ;; Assertion failures in `cargo test' runs.
             (panic (: (group-n 4 "panicked") " at " loc)))

      (setf (alist-get 'rust-diags compilation-error-regexp-alist-alist)
            `(,(rx diag) 1 2 3 (4 . 5) 6))
      (setf (alist-get 'rust-secondary compilation-error-regexp-alist-alist)
            `(,(rx secondary) 1 2 3 0 6))
      (setf (alist-get 'rust-snippet-line compilation-error-regexp-alist-alist)
            `(,(rx snippet-line) nil 2 nil 0 2))
      (setf (alist-get 'rust-panics compilation-error-regexp-alist-alist)
            `(,(rx panic) 1 2 3 nil 6 (4 'compilation-error)))
      ;; Since snippet lines do not include a filename, ordinary diags have to
      ;; be processed first.  I.e., the order in this alist is significant.
      (add-to-list 'compilation-error-regexp-alist 'rust-diags 'append)
      (add-to-list 'compilation-error-regexp-alist 'rust-secondary 'append)
      (add-to-list 'compilation-error-regexp-alist 'rust-snippet-line 'append)
      (add-to-list 'compilation-error-regexp-alist 'rust-panics 'append))))

(use-package uniquify
  :demand t

  :custom
  (uniquify-buffer-name-style 'post-forward-angle-brackets)
  (uniquify-ignore-buffers-re "^\\*")
  (uniquify-after-kill-buffer-p t))

(use-package url-cookie
  :no-require t

  :custom
  (url-cookie-confirmation t))

(use-package url-vars
  :no-require t

  :custom
  (url-privacy-level 'paranoid))

(use-package vc
  :defer t

  :custom
  (vc-handled-backends '(Git Hg))

  :init
  (with-eval-after-load 'evil-core
    (add-to-list 'evil-buffer-regexps
                 '("\\`\\*vc-diff\\*" . emacs))))

(use-package vertico
  :demand t

  :gfhook ('minibuffer-setup-hook #'vertico-repeat-save)

  :general
  ("M-_" 'vertico-suspend)
  (my:general/leader
    "RET" 'vertico-repeat-select)
  :general-config
  ( :keymaps 'vertico-map
    "M-RET" 'vertico-exit-input
    "RET" 'vertico-directory-enter
    "DEL" 'vertico-directory-delete-char
    "M-DEL" 'vertico-directory-delete-word
    "<next>" 'scroll-up-command
    "<prior>" 'scroll-down-command)

  :custom
  (vertico-mode t)

  :config
  (define-advice ffap-menu-ask
      (:around (&rest args) no-completions-buffer)
    "Suppress the completions buffer for `ffmap-menu'."
    (cl-letf (((symbol-function #'minibuffer-completion-help)
               #'ignore))
      (apply args)))

  (define-advice vertico--setup
      (:after (&rest _args) disable-completion-messages)
    "Disable completion messages of `minibuffer' commands."
    (setq-local completion-auto-help nil
                completion-show-inline-help nil)))

(use-package vundo
  :defer t

  :general
  (my:general/leader "u" 'vundo)

  :custom
  (evil-undo-system 'undo-redo)
  (vundo-glyph-alist
   '((selected-node . ?⨯)
     (node . ?•)
     (horizontal-stem . ?╌)
     (vertical-stem . ?╎)
     (branch . ?├)
     (last-branch . ?└)))

  :config
  (evil-set-initial-state 'vundo-diff-mode 'emacs))

(use-package which-func
  :defer t

  :custom
  (which-function-mode t)

  :config
  ;; TODO: Already done in library itself, but does not seem to have an effect?
  (add-hook 'after-change-major-mode-hook #'which-func-ff-hook t))

(use-package whitespace
  :defer t
  :diminish ""

  :custom
  (global-whitespace-mode t)
  (whitespace-line-column nil)          ; Use value of fill-column
  (whitespace-style '(face trailing lines-tail space-before-tab empty
                           missing-newline-at-eof))
  (whitespace-display-mappings
   '((space-mark   ?\     [?.])
     (space-mark   ?\xA0  [?~])         ; no-break space
     (newline-mark ?\n    [?\u2b90 ?\n] [?$ ?\n])
     ;; FIXME: leads to a spurious tab when a tab occupies exactly one column
     (tab-mark     ?\t    [?\u25b8 ?\t] [?\\ ?\t])))

  :config
  (defun whitespace-enable-predicate@only-enable-in-prog-mode (enable)
    "Only enable `global-whitespace-mode' for modes derived from `prog-mode'.

  The original definition of the predicate already takes care to
  disable global whitespace mode for special buffers (like those
  used for fontifying Org code blocks). (That is also the
  motivation in using global whitespace mode instead of adding
  local whitespace mode to the `prog-mode-hook': It would then be
  turned on unconditionally."
    (and enable (derived-mode-p 'prog-mode)))
  (add-function :filter-return
                (var whitespace-enable-predicate)
                #'whitespace-enable-predicate@only-enable-in-prog-mode))

(defun set-whitespace-line-column+ (value)
  "Set buffer-local value of `whitespace-line-column' to VALUE.
When called interactively with \\[universal-argument] the
buffer-local setting is removed."
  (interactive
   (list (if current-prefix-arg nil
           (read-number "Set whitespace-line-column to: "
                        (cond (whitespace-line-column)
                              ((> (current-column) 0) (current-column))
                              (fill-column))))))
  (message "Whitespace line column set to %s (was %s)"
           value whitespace-line-column)
  (if value
      (setq-local whitespace-line-column value)
    (kill-local-variable 'whitespace-line-column))

  ;; Refresh highlighting but keep buffer-local styles.
  (when (bound-and-true-p whitespace-active-style)
    (let ((whitespace-style whitespace-active-style))
      (whitespace-turn-off)
      (whitespace-turn-on))))

(defun whitespace-toggle-styles+ (&optional force extra-styles)
  "Toggle marking of EXTRA-STYLES whitespace styles.
If FORCE is zero or negative, the extra styles are disabled unconditionally.
Else if FORCE is non-nil, the extra styles are enabled unconditionally."
  (interactive "P")
  (unless extra-styles
    (setq extra-styles
          '(spaces space-mark tabs tab-mark newline newline-mark)))

  (whitespace-turn-off)
  (let* ((style (or whitespace-active-style whitespace-style))
         (disable
          (if (not force)
              (cl-intersection extra-styles style)
            (<= (prefix-numeric-value force) 0)))
         (whitespace-style
          (if disable
              (cl-set-difference style extra-styles)
            (cl-union style extra-styles))))
    (whitespace-turn-on)))

(defun hooks:toggle-whitespace-style ()
  "Toggle whitespace style to mark newlines in `visual-line-mode'."
  (unless (derived-mode-p 'special-mode)
    (let ((to-disable (cl-intersection '(lines lines-tail) whitespace-style))
          (to-enable '(newline newline-mark)))
      (whitespace-toggle-styles+ (if visual-line-mode -1 +1) to-disable)
      (whitespace-toggle-styles+ (if visual-line-mode +1 -1) to-enable))))
(add-hook 'visual-line-mode-hook #'hooks:toggle-whitespace-style)

(my:general/leader
  "fs" #'whitespace-toggle-styles+
  "fo" #'whitespace-toggle-options
  "fw" #'set-whitespace-line-column+
  "ff" #'set-fill-column
  "v" #'visual-line-mode)

(use-package whitespace-cleanup-mode
  :defer t
  :diminish ""
  :hook prog-mode)

(defun split-window-extra-sensibly+ (&optional window)
  "Split WINDOW depending on the orientation of its frame."
  (interactive)
  (setq window (or window (selected-window)))
  ;; HACK: The logic in `window-splittable-p' depends on `window-width', whose
  ;; result does not include "vertical dividers, fringes or maginal areas or
  ;; scroll bars".  In the case of magit log buffers this leads to a
  ;; considerably reduced width even for windows spanning the full frame.
  ;; Instead use `window-total-width', which might produce wrong results in
  ;; some different situation, but so be it.
  (cl-letf* (((symbol-function 'window-width) 'window-total-width)
             ((symbol-function 'window-body-width) 'window-total-width)
             (v-split-okay (window-splittable-p window))
             (h-split-okay (window-splittable-p window 'horizontal))
             (frame (window-frame window))
             (is-portrait (< (frame-width frame) (frame-height frame)))
             (prefer-vertical-split is-portrait))
    (or
     (with-selected-window window
       (cond ((and prefer-vertical-split v-split-okay)
              (split-window-vertically))
             (h-split-okay
              (split-window-horizontally))
             (v-split-okay
              (split-window-vertically))))
     ;; Fall back to default implementation.
     (split-window-sensibly window))))

(use-package window
  :defer t

  :custom
  (split-width-threshold 120)
  (split-height-threshold 60)
  (split-window-preferred-function #'split-window-extra-sensibly+))

(use-package winner
  :defer t

  :custom
  (winner-mode t))

(use-package xref
  :defer t

  :custom
  (xref-history-storage #'xref-window-local-history))

(use-package xt-mouse
  :demand t

  :custom
  (xterm-mouse-mode t))

(use-package yaml-mode
  :defer t

  :mode ("\\.clang-\\(?:format\\|tidy\\)\\'" . yaml-mode))

(use-package yasnippet
  :diminish yas-minor-mode
  :defer 4

  :custom
  (yas-prompt-functions '(yas-ido-prompt
                          yas-completing-prompt))
  ;; do not use snippets shipped with yasnippet
  (yas-snippet-dirs
   (list (no-littering-expand-etc-file-name "yasnippet/snippets/")))
  (yas-triggers-in-field t)

  :config
  (defun my:yas/basename-or (default)
    (file-name-nondirectory
     (file-name-sans-extension
      (or (buffer-file-name) default))))

  (defun my:yas/inside-comment-p ()
    (or (nth 4 (syntax-ppss))
        (memq (get-text-property (point) 'face)
              '(font-lock-comment-face font-lock-comment-delimiter-face))))

  (defun my:yas/nl ()
    (when (yas-text) "\n"))

  (defun my:yas/space (&optional length)
    (when (yas-text) (make-string (or length 1) ? )))

  (defun my:yas/last-word ()
    (yas-substr yas-text "[^ \t]+\\'"))

  (defun my:yas/c++-class-name-or (default)
    (save-excursion
      (beginning-of-line)
      (beginning-of-defun)
      (if (re-search-forward
           (rx (or "class" "struct")
               (* space)
               (group (+ (not (any space ":")))))
           nil t)
          (match-string-no-properties 1)
        default)))

  (defun hooks:yasnippet/relative-indentation ()
    "Always indent snippets relative to the current column."
    (setq-local yas-indent-line 'fixed))
  (add-hook 'python-mode-hook #'hooks:yasnippet/relative-indentation)

  (yas-global-mode 1))



(use-package org
  :mode ("\\.org\\(_archive\\)?\\'" . org-mode)
  :defer t

  :general
  ("C-c c" 'org-capture
   "C-c n" 'org-clock-add-note+)
  (my:general/leader
    "a" 'org-agenda
    "A" 'org-agenda-redisplay-last+
    "c" 'org-capture
    "C" 'org-capture-goto-last-stored
    "O" 'org-switchb
    "l" 'org-store-link)
  :general-config
  (general-nvmap :keymaps 'org-mode-map
    "RET" 'org-open-at-point-same-window+

    "g RET" 'org-open-at-point

    "gt" 'org-todo
    "gT" 'org-todo-with-note+
    "g:" 'org-set-tags-command
    "gP" 'org-set-property

    "zu" 'org-up-element
    "zd" 'org-down-element
    "zh" 'org-mark-element

    "gs" 'org-sort
    "gr" 'org-refile
    "gR" 'org-refile-goto-last-stored

    "gzg" 'org-clock-goto
    "gzi" 'org-clock-in
    "gzo" 'org-clock-out
    "gzc" 'org-clock-cancel
    "gzl" 'org-clock-in-last
    "gzn" 'org-add-note
    "gze" 'org-set-effort
    "gzr" 'org-resolve-clocks

    "Z" 'org-speed-command-execute+)
  (:keymaps 'org-agenda-mode-map
            "C-c [" 'org-agenda-file-to-front
            "C-c ]" 'org-remove-file
            "C-c t" 'epoch-agenda-todo
            "C->" 'org-set-last-review-property+
            "C-." 'org-agenda-filter-by-last-review+)
  (:keymaps 'org-mode-map
            "C-c C-x s" 'org-insert-sources-block+
            "C->" 'org-set-last-review-property+)

  :custom
  (org-hide-leading-stars nil)
  (org-startup-folded t)
  (org-startup-indented nil)
  (org-startup-with-inline-images t)
  (org-hide-block-startup t)
  (org-adapt-indentation nil)
  (org-fold-catch-invisible-edits 'smart)
  (org-use-sub-superscripts nil)
  (org-ellipsis "···")
  (org-cycle-level-faces nil)

  (org-priority-highest ?A)
  (org-priority-default ?D)
  (org-priority-lowest ?F)

  (org-src-fontify-natively t)
  (org-fontify-done-headline t)
  (org-pretty-entities t)
  (org-pretty-entities-include-sub-superscripts nil)
  (org-highlight-latex-and-related '(latex))

  (org-cycle-separator-lines -1)
  (org-blank-before-new-entry
   '((heading . auto)
     (plain-list-item)))

  (org-cycle-global-at-bob t)
  (org-special-ctrl-a/e t)
  (org-special-ctrl-k t)
  (org-insert-heading-respect-content nil)
  (org-ctrl-k-protect-subtree t)
  (org-yank-adjusted-subtrees t)
  ;; FIXME: yas feedback loop
  ;; (org-src-tab-acts-natively t)

  (org-clone-delete-id t)
  (org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)
  (org-id-link-consider-parent-id t)

  (org-edit-src-content-indentation 0)
  (org-list-demote-modify-bullet '(("+" . "-") ("-" . "+") ("*" . "+")))
  (org-list-allow-alphabetical t)

  (org-loop-over-headlines-in-active-region 'start-level)

  (org-outline-path-complete-in-steps nil)

  (org-directory "~/org/")
  (org-default-notes-file
   (expand-file-name
    (concat "refile-" (s-dashed-words (system-name)) ".org") org-directory))
  ;; Inhibit startup, since buffers will be temporarily loaded in the
  ;; background for `org-agenda-to-appt', which is expensive.
  (org-agenda-inhibit-startup t)
  (org-agenda-files (list org-directory))
  (org-agenda-file-regexp "\\`[^_.].*[^_]\\.org\\'")
  (org-agenda-skip-unavailable-files t)
  (org-agenda-diary-file
   (if (or
        (executable-find "gpg2")
        (executable-find "gpg"))
       (expand-file-name "journal.org.gpg" org-directory)
     org-default-notes-file))

  (org-link-email-description-format "Email %c: %.30s (%d)")

  ;; tags & todo keywords

  (org-tag-persistent-alist
   '((:startgroup)
     ("@errands" . ?e)
     ("@home" . ?h)
     ("@work" . ?w)
     ("@online" . ?o)
     (:endgroup)
     ("#msg" . ?m)
     ("#call" . ?c)
     ("#idle" . ?i)
     ("noexport" . ?X)
     ("FLAGGED" . ??)))

  (org-complete-tags-always-offer-all-agenda-tags nil)

  (org-treat-S-cursor-todo-selection-as-state-change nil)

  (org-log-done 'time)
  (org-log-redeadline 'time)
  (org-log-reschedule 'time)
  (org-log-into-drawer t)
  (org-todo-keywords
   '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d/!)")
     (type "PROJ(p)" "|")
     (sequence "WAITING(w@/!)" "INACTIVE(i@/!)" "|" "CANCELLED(c@/!)")))

  (org-enforce-todo-dependencies t)

  (org-todo-keyword-faces
   '(("TODO" . my:org/todo-face)
     ("NEXT" . my:org/next-face)
     ("DONE" . my:org/done-face)
     ("PROJ" . my:org/project-face)
     ("WAITING" . my:org/waiting-face)
     ("INACTIVE" . my:org/inactive-face)
     ("CANCELLED" . my:org/cancelled-face)))

  ;; refiling

  (org-goto-interface 'outline-path-completion)
  (org-goto-max-level 10)
  (org-refile-use-cache t)
  (org-refile-targets
   '((nil :maxlevel . 10)
     (my:org/list-open-org-files :maxlevel . 5)
     (org-agenda-files :maxlevel . 3)))
  (org-refile-target-verify-function 'my:org/refile-verify-target)
  (org-refile-use-outline-path 'buffer-name)
  (org-refile-allow-creating-parent-nodes 'confirm)

  ;; clocking & properties

  (org-read-date-prefer-future 'time)
  (org-global-properties
   '(("Effort_ALL" . "0:05 0:10 0:15 0:30 0:45 1:00 2:00 4:00 6:00 0:00 :ETC")
     ("STYLE_ALL" . "habit :ETC")))
  (org-columns-default-format
   "%60ITEM(Task) %10Effort(Effort){:} %10CLOCKSUM(Clock Σ)")

  ;; exporting

  (org-babel-load-languages '((emacs-lisp . t)
                              (python . t)))

  (org-modules nil)

  :config
  (defface my:org/todo-face
    '((t (:inherit org-todo :bold t :foreground "salmon"))) "" :group 'org-faces)
  (defface my:org/next-face
    '((t (:inherit org-todo :bold t :foreground "light green"))) "" :group 'org-faces)
  (defface my:org/project-face
    '((t (:inherit org-todo :bold t :foreground "blue violet"))) "" :group 'org-faces)
  (defface my:org/waiting-face
    '((t (:inherit org-todo :bold t :foreground "light sea green"))) "" :group 'org-faces)
  (defface my:org/inactive-face
    '((t (:inherit org-todo :bold t :foreground "thistle"))) "" :group 'org-faces)
  (defface my:org/done-face
    '((t (:inherit org-done :bold t :foreground "cadet blue"))) "" :group 'org-faces)
  (defface my:org/cancelled-face
    '((t (:inherit org-done :bold t :foreground "pale violet red"))) "" :group 'org-faces)

  (defun org-todo-with-note+ ()
    "Change the TODO state of an item, forcing note taking."
    (interactive)
    (let ((org-todo-log-states
           (cl-loop for state in org-todo-keywords-1
                    collect (list state 'note 'time))))
      (call-interactively 'org-todo)))

  (defun hooks:org/tag-from-keyword (change-plist)
    (let ((type (plist-get change-plist :type))
          (from (plist-get change-plist :from))
          ;; `org-trigger-hook' is run at the end of `org-todo', after
          ;; changing the state and after `org-auto-repeat-maybe' has
          ;; been called.  The latter might have reverted the state
          ;; change.  Consequently use the actual tag, not the one
          ;; specified in the change list.
          (to (org-get-todo-state))
          ;; For each of the following states always add a tag of the same name.  This is used for
          ;; querying (e.g. hiding items/trees in the agenda), as tags are inherited.
          (keywords-to-tag '("WAITING" "INACTIVE" "CANCELLED")))
      (when (eql type 'todo-state-change)
        (when (member from keywords-to-tag)
          (org-toggle-tag from 'off))
        (when (member to keywords-to-tag)
          (org-toggle-tag to 'on))
        ;; Projects are always tagged with "PROJECT", such that they can be set to DONE, WAITING, etc.
        ;; while still being recognized as a project.  However upon switching to a regular TODO
        ;; or NEXT keyword the project tag is removed.
        (cond ((member to '(nil "TODO" "NEXT"))
               (org-toggle-tag "PROJECT" 'off))
              ((member "PROJ" (list to from))
               (org-toggle-tag "PROJECT" 'on)))))
    (org-align-tags))
  (add-hook 'org-trigger-hook #'hooks:org/tag-from-keyword)

  (defun my:org/list-open-org-files ()
    (delq nil (mapcar (lambda (buffer)
                        (when (eql (buffer-local-value 'major-mode buffer)
                                  'org-mode)
                          (buffer-file-name buffer)))
                      (buffer-list))))

  (defun my:org/refile-verify-target ()
    "Exclude `org-default-notes-file' and done tasks from refile targets."
    (not (or (f-same? (buffer-file-name) org-default-notes-file)
             (f-ancestor-of? (expand-file-name "archive/" org-directory)
                             (or (buffer-file-name) ""))
             (member (nth 2 (org-heading-components)) org-done-keywords))))

  (defun org-agenda-redisplay-last+ ()
    ""
    (interactive)
    (let ((win (get-buffer-window org-agenda-buffer)))
      (if win
          (select-window win)
        (pop-to-buffer org-agenda-buffer))))

  (defun my:org/agenda-visit-buffer-then-call (fun &rest args)
    (with-current-buffer
        (if (not (derived-mode-p 'org-agenda-mode))
            (current-buffer)
          (marker-buffer (or (org-get-at-bol 'org-marker)
                             (org-agenda-error))))
      (apply fun args)))
  (advice-add 'org-agenda-file-to-front
              :around 'my:org/agenda-visit-buffer-then-call)
  (advice-add 'org-remove-file
              :around 'my:org/agenda-visit-buffer-then-call)

  (defun org-infer-date-for-datetree ()
    "Search upwards for a headline containing a timestamp or starting with a date."
    (save-excursion
      (let ((headline-with-date
             (rx (* (any "* \t"))
                 (group (= 4 digit) (? "-")
                        (= 2 digit) (? "-") (= 2 digit))))
            (has-parents t)
            (datetree-date))
        (org-back-to-heading)
        (catch 'found-date
          (while has-parents
            (setq datetree-date
                  (or (org-entry-get nil "TIMESTAMP" t)
                      (org-entry-get nil "TIMESTAMP_IA" t)
                      (when (looking-at headline-with-date)
                        (match-string 1))))
            (when datetree-date
              (throw 'found-date (org-date-to-gregorian datetree-date)))
            (setq has-parents (org-up-heading-safe)))))))

  (defun org-refile-to-diary+ (&optional pick-date filename)
    "Move the entry or entries at point to the date tree in `org-agenda-diary-file'.

With `\\[universal-argument]' prompt for the date to use for filing the entry.
With `\\[universal-argument] \\[universal-argument]' use the date tree in the current buffer."
    (interactive (cl-case (prefix-numeric-value current-prefix-arg)
                   (4 '(t nil))
                   (16 (list t (buffer-file-name)))))
    (let* ((org-read-date-prefer-future nil)
           (calendar-date-display-form calendar-iso-date-display-form)
           (date (org-infer-date-for-datetree))
           (default-date-string (if date (calendar-date-string date) "")))
      (when (or pick-date (not date))
        (setq date (org-date-to-gregorian
                    (org-read-date
                     nil nil nil nil nil default-date-string))))
      (let* ((file (or filename org-agenda-diary-file))
             (loc (save-excursion
                    (with-current-buffer (or (find-buffer-visiting file)
                                             (find-file-noselect file))
                      (org-datetree-find-iso-week-create date)
                      (point)))))
        (org-refile nil nil (list "datetree" file nil loc)))))

  (org-clock-persistence-insinuate)

  (defun hooks:org/setup-spell-checking ()
    (unless org-inhibit-startup
      (with-demoted-errors "Failed to change dictionary: %S" (my:org/ispell-change-dictionary))))
  (add-hook 'org-mode-hook #'hooks:org/setup-spell-checking)

  (defface org-link-id+ '((t :inherit org-link))
    "Face for id links.")

  (defface org-link-roam+ '((t :inherit org-link))
    "Face for roam id links.")

  (defface org-link-invalid+ '((t :inherit org-link))
    "Face for invalid id links.")

  (defun my:org/id-link-face (path)
    (let ((id (if (string-match "\\(.+\\)::.+" path)
                  (match-string 1 path)
                path)))
      (cond ((and (fboundp 'org-roam-db-query)
                  (org-roam-db-query [:select [level] :from nodes
                                              :where (= id $s1)
                                              :limit 1]
                                     id))
             'org-link-roam+)
            (org-id-track-globally
             (if (org-id-find id)         ; TODO: Maybe too expensive?
                 'org-link-id+
               'org-link-invalid+))
            (t 'org-link))))

  (org-link-set-parameters "id" :face 'my:org/id-link-face)

  (defun org-insert-sources-block+ ()
    "Insert a `begin_sources' block at point."
    (interactive)
    (org-insert-structure-template "sources"))

  (defun my:org/ispell-change-dictionary ()
    "Change ispell dictionary according to #+LANGUAGE keyword."
    (org-with-wide-buffer
     (goto-char (point-min))
     (let ((case-fold-search t))
       (when (re-search-forward "^[ \t]*#\\+LANGUAGE:" nil t)
         (when-let ((element (org-element-at-point))
                    (is-keyword (eql (org-element-type element) 'keyword))
                    (dictionary (assoc-default
                                 (org-element-property :value element)
                                 '(("de" . "german")
                                   ("de-de" . "german")
                                   ("en" . "default")))))
           (unless (string-equal ispell-local-dictionary dictionary)
             (ispell-change-dictionary dictionary)
             t))))))

  (defun my:org/from-timestamp (fun)
    (if (not (org-at-timestamp-p t))
        (user-error "Not at a timestamp")
      (let ((beg (match-beginning 0))
            (end (match-end 0))
            (time (replace-regexp-in-string "\\`[[<]\\|[]>]\\'" ""
                                            (match-string-no-properties 0))))
        (delete-region beg end)
        (delete-trailing-whitespace
         (line-beginning-position) (line-end-position))
        (funcall fun nil time))))

  (defun org-open-at-point-same-window+ (&optional arg)
    (interactive "P")
    (let ((org-link-frame-setup
           (cl-acons 'file 'find-file org-link-frame-setup)))
      (org-open-at-point arg)))

  (defun org-set-deadline-from-timestamp+ ()
    (interactive)
    (my:org/from-timestamp #'org-deadline))

  (defun org-set-schedule-from-timestamp+ ()
    (interactive)
    (my:org/from-timestamp #'org-schedule))

  (defun org-speed-command-execute+ (key)
    (interactive
     (let ((key (read-key "Speed command (? for help):")))
       (list (if (characterp key) (char-to-string key) "?"))))
    (let ((org-use-speed-commands t)
          (org-speed-command (cdr (assoc key org-speed-commands))))
      (cond
       ((commandp org-speed-command)
        (setq this-command org-speed-command)
        (call-interactively org-speed-command))
       ((functionp org-speed-command)
        (funcall org-speed-command))
       ((and org-speed-command (listp org-speed-command))
        (eval org-speed-command)))))

  (defun org-electric-dollar+ ()
    "Add inline LaTeX math delimiters.
Insert `\\(\\)' and leave point in between.  If called a second time,
replace surrounding `\\(\\)' by a single `$'.  If called in front of `\\)',
skip over it."
    (interactive)
    (cond ((not (looking-at "\\\\)"))
           (insert "\\(\\)")
           (backward-char 2))
          ((looking-back "\\\\(" (save-excursion (forward-line 0)))
           (delete-char 2)
           (delete-char -2)
           (insert "$"))
          (t (forward-char 2))))

  (define-key org-mode-map "$" 'org-electric-dollar+))

(use-package org-agenda
  :no-require t
  :after org

  :custom
  (org-stuck-projects '("TODO=\"PROJ\"" ("NEXT" "WAITING") nil ""))
  (org-agenda-skip-deadline-if-done t)
  (org-agenda-skip-scheduled-if-done t)
  (org-agenda-skip-timestamp-if-done t)
  (org-agenda-todo-ignore-scheduled 'future)
  (org-agenda-tags-todo-honor-ignore-options t)
  (org-agenda-persistent-filter t)
  (org-agenda-block-separator ?─)
  (org-agenda-compact-blocks nil)
  (org-agenda-dim-blocked-tasks nil)
  (org-agenda-clockreport-parameter-plist
   '(:link t :maxlevel 5 :fileskip0 t :compact t :narrow 80))
  (org-agenda-time-grid
   '((daily today require-timed)
     (0900 1100 1400 1600 1800)
     " · · ·"
     "----------------"))
  (org-agenda-span 3)
  (org-agenda-sticky nil)
  (org-agenda-skip-additional-timestamps-same-entry nil)
  (org-agenda-window-setup 'other-window)
  (org-agenda-restore-windows-after-quit t)
  (org-agenda-text-search-extra-files '(agenda-archives))

  (org-agenda-tags-column -76)    ; 'auto
  (org-agenda-show-inherited-tags nil)
  (org-agenda-prefix-format
   '((agenda  . " %i %?-12t%20 s")
     (todo  . " %i ")
     (tags  . " %i ")
     (search . " %i ")))

  (org-agenda-exporter-settings
   '((ps-landscape-mode t)
     (ps-number-of-columns 2)
     (ps-print-header t)
     (ps-print-header-frame nil)
     (ps-print-only-one-header t)
     (ps-paper-type 'a4)
     (ps-print-color-p nil)
     (org-agenda-add-entry-text-maxlines 5)
     (ps-left-header '(buffer-name ps-header-dirpart))))

  (org-agenda-bulk-custom-functions
   '((?R org-set-last-review-property+)))

  :config
  (add-hook 'org-agenda-mode-hook 'hl-line-mode 'append)

  (cl-letf ((projects
             '(tags-todo "TODO=\"PROJ\"|PROJECT"
                         ((org-agenda-overriding-header "Projects")
                          (org-use-tag-inheritance nil))))
            (next-actions
             '(tags-todo "TODO=\"NEXT\"-INACTIVE-WAITING-CANCELLED|TODO=\"TODO\"-PROJECT-INACTIVE-WAITING-CANCELLED"
                         ((org-agenda-overriding-header "Next Actions"))))
            (someday
             `(tags-todo "TODO=\"PROJ\"|-PROJECT|LEVEL=1"
                         ((org-agenda-overriding-header "Someday / Maybe")
                          (org-agenda-files `(,,(expand-file-name "_someday_.org" org-directory)))
                          (org-agenda-file-regexp ".*"))))
            (upcoming
             '(agenda
               ""
               ((org-agenda-overriding-header "Upcoming 7 Days")
                (org-agenda-start-on-weekday nil)
                (org-agenda-span 'week))))
            (waiting
             '(todo "WAITING" ((org-agenda-overriding-header "Waiting"))))
            (inactive
             '(todo "INACTIVE" ((org-agenda-overriding-header "Inactive"))))
            ((symbol-function 'tasks)
             (lambda (title match)
               (list title 'tags-todo
                     (concat match "+PROJECT+TODO=\"NEXT\"|"
                             match "-PROJECT"
                             "/!-PROJ-WAITING-INACTIVE")
                     `((org-agenda-overriding-header ,title))))))
    (setq
     org-agenda-custom-commands
     `(("p" "Projects" ,@projects)
       ("n" "Next Actions" ,@next-actions)
       ("A" "Tasks to Archive" tags "/"
        ((org-agenda-skip-function #'my:org/skip-non-archivable-tasks)
         (org-agenda-overriding-header "Tasks to Archive")
         (org-agenda-span "+2w")
         (org-tags-match-list-sublevels nil)))
       ("L" "Timeline"
        agenda
        ""
        ((org-agenda-start-on-weekday nil)
         (org-agenda-start-day "-3d") ; on Monday: show Friday
         (org-agenda-span 8)
         (org-agenda-overriding-header "Timeline")
         (org-agenda-start-with-log-mode t)
         (org-agenda-prefix-format " %i %-12:c%?-12t% s")))
       ("u" "Upcoming Week" ,@upcoming)
       ("w" "Waiting" ,@waiting)
       ("i" "Inactive" ,@inactive)
       ("r" "Weekly Review"
        ((,@upcoming)
         (,@next-actions)
         (,@waiting)
         (,@projects)
         (stuck "" ((org-agenda-overriding-header "Stuck Projects")))
         (,@inactive)
         (,@someday)))
       ("c" . "Context-specific agendas")
       ("cc" ,@(tasks "Call" "#call"))
       ("cm" ,@(tasks "Message" "#msg"))
       ("ci" ,@(tasks "When Idle" "#idle"))
       ("l" . "Location-specific agendas")
       ("l " ,@(tasks "Anywhere" "-{^@.*}"))
       ("lh" ,@(tasks "At Home" "@home"))
       ("lw" ,@(tasks "At Work" "@work"))
       ("le" ,@(tasks "When running errands" "@errands"))
       ("lo" ,@(tasks "Online" "@online")))))

  (define-advice org-agenda-set-mode-name (:after () diminish)
    (when (< (window-width) 90)
      (setq mode-name (mapcar (lambda (elt)
                                (if (stringp elt)
                                    (string-trim elt)
                                  elt))
                              mode-name)))
    (when-let ((snooze org-agenda-last-review-filter))
      (setq snooze (replace-regexp-in-string
                    " " "" (org-duration-from-minutes snooze) t t))
      (setq mode-name (append mode-name (list (format " %s…" snooze)))))
    (setq mode-name (delete " " (delete "" (delete "Org-Agenda" mode-name))))
    (force-mode-line-update))

  (define-advice org-agenda-prepare-window (:around (fun &rest args) respect-frame)
    (let ((org-agenda-window-setup (if (my:frame-small-p)
                                       'only-window
                                     'other-window)))
      (apply fun args)))

  (defun my:org/skip-non-archivable-tasks ()
    "Skip subtrees that are not yet available for archiving.

Only tasks marked as done that are old enough are considered for
archiving.  Any task that has a 'KEEP_UNTIL' property is kept
until that date.  Relative times are calculated based on the date
stored in the 'CLOSED' property.  If no value for 'KEEP_UNTIL' is
set, the value of ´org-agenda-span' is used as the grace period."
    (let* ((grace-period (or org-agenda-current-span org-agenda-span))
           (org-expiry-wait
            (pcase grace-period
              ('day "+1d")
              ('week "+1w")
	      ('fortnight "+2w")
              ('month "+1m")
              ('year "+1y")
              ((pred stringp)
               (unless (eql (string-to-char grace-period) ?+)
                 (error "Grace period should begin with a \"+\", not %S" grace-period))
               grace-period)
              ((pred numberp)
               (format "+%dd" grace-period))
              (_ (error "Value not supported for grace period: %S" grace-period))))
           (org-expiry-created-property-name "CLOSED")
           (org-expiry-expiry-property-name "KEEP_UNTIL")
           (todo-state (org-get-todo-state))
           (is-task (member todo-state org-todo-keywords-1))
           (is-done (and is-task (member todo-state org-done-keywords)))
           (is-expired (and is-done (org-expiry-expired-p)))
           (is-inlinetask (and (fboundp 'org-inlinetask-at-task-p)
                               (org-inlinetask-at-task-p)))
           (should-skip-inlinetask is-inlinetask) ; Always skip inlinetasks.
           (should-skip-heading (not is-task))
           (should-skip-subtree (not is-expired)))
      (save-excursion
        (save-restriction
          (widen)
          (cond (should-skip-inlinetask
                 (org-inlinetask-goto-end)
                 (point))
                (should-skip-heading
                 (or (outline-next-heading)
                     (point-max)))
                (should-skip-subtree
                 (or (org-end-of-subtree 'invisible-ok)
                     (point-max))))))))

  (defconst my:org/last-review-property "LAST-REVIEW")
  (defun org-set-last-review-property+ ()
    "Set the 'last-review' property of the current entry."
    (interactive)
    (save-window-excursion
      (when (eql major-mode 'org-agenda-mode)
        (org-agenda-goto))
      (let ((org-expiry-expiry-property-name my:org/last-review-property))
        (org-expiry-insert-expiry 'now)))
    (message "%s property set" my:org/last-review-property))

  ;; Unfortunately it's not possible to add this to `org-agenda-local-vars'.
  (defvar-local org-agenda-last-review-filter nil)
  (defun org-agenda-filter-by-last-review+ ()
    "Filter lines in the agenda buffer that were reviewed recently."
    (interactive)
    (let* ((snooze org-agenda-last-review-filter)
           (snooze (read-string
                    "Snooze interval after review: "
                    (and snooze (org-duration-from-minutes snooze)))))
      (org-agenda-filter-by-last-review-apply snooze)))

  (defun org-agenda-filter-by-last-review-apply (snooze)
    "Filter lines in the agenda buffer that were reviewed recently."
    (when (stringp snooze)
      (setq snooze (org-duration-to-minutes snooze)))
    (unless (> snooze 0)
      (user-error "Snooze interval should be positive"))
    (when org-agenda-entry-text-mode (org-agenda-entry-text-mode))
    (org-agenda-remove-filter 'last-review)
    (save-excursion
      (goto-char (point-min))
      (while (not (eobp))
        (when-let ((marker (org-get-at-bol 'org-hd-marker))
                   (buffer (marker-buffer marker))
                   (keep t))
          (with-current-buffer buffer
            (org-with-wide-buffer
             (goto-char marker)
             (when-let ((prop my:org/last-review-property)
                        (prop-val (org-entry-get (point) prop))
                        (last-reviewed (org-read-date nil t prop-val))
                        (now (current-time))
                        (snoozed-until
                         (time-add last-reviewed
                                   (seconds-to-time (* 60.0 snooze)))))
               (let ((not-reviewed-recently
                      (time-less-p snoozed-until now)))
                 (setq keep not-reviewed-recently)))))
          (unless keep
            (org-agenda-filter-hide-line 'last-review)))
        (beginning-of-line 2)))
    (when (get-char-property (point) 'invisible)
      (org-agenda-previous-line))
    (setq org-agenda-last-review-filter snooze)
    (org-agenda-set-mode-name)
    (message "Hiding entries reviewed in the last %s"
             (org-duration-from-minutes snooze)))

  (define-advice org-agenda-quit (:after (&rest _args) delete-frame)
    (when (string-equal (frame-parameter nil 'name) "emacs-popup")
      (delete-frame nil 'force))))

(use-package org-archive
  :no-require t
  :after org

  :custom
  (org-archive-mark-done nil)
  (org-archive-location "archive/%s::"))

(use-package org-attach
  :no-require t
  :after org

  :custom
  (org-attach-auto-tag nil)
  (org-attach-store-link-p 'attached))

(use-package org-capture
  :no-require t
  :after org

  :custom
  (org-capture-templates
   `(("t" "to do" entry (file "")
      "* TODO %?\n")
     ("T" "to do tomorrow" entry (file "")
      "* TODO %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+1d\"))\n")
     ("f" "follow-up" entry (file "")
      "* TODO Follow-up to %:fromname on %:subject\nSCHEDULED: %t\n[[%:link][%:subject]] received on %:date-timestamp-inactive\n" :immediate-finish t)
     ("s" "snippet / quote" entry (file "")
      "* %:fromname on \"%:subject\" :quote:\n#+BEGIN_QUOTE\n%i\n#+END_QUOTE\n[[%:link][%:subject]] received on %:date-timestamp-inactive\n")
     ("n" "note" entry (file "")
      "* %?\n%i\n")
     ("j" "Journal" entry (file+olp+datetree ,org-agenda-diary-file)
      "* %?\n" :tree-type week)
     ("J" "Journal (pick date)" entry
      (file+olp+datetree ,org-agenda-diary-file)
      "* %?\n" :tree-type week :time-prompt t)
     ("r" "Review: Daily/Weekly/Monthly")
     ("rd" "Review: Daily Review" entry
      (file+olp+datetree ,org-agenda-diary-file)
      (file "templates/daily-review.org")
      :tree-type week :immediate-finish t :jump-to-captured t)
     ("rw" "Review: Weekly Review" entry
      (file+olp+datetree ,org-agenda-diary-file)
      (file "templates/weekly-review.org")
      :tree-type week :immediate-finish t :jump-to-captured t)
     ("rt" "Review: Incompletion Triggers" entry
      (file+olp+datetree ,org-agenda-diary-file)
      (file "templates/triggers.org")
      :tree-type week :immediate-finish t :jump-to-captured t)
     ("p" "org-protocol"
      plain (function my:org/open-buffer-for-capture-to-kill-ring)
      ,(concat "%(my:org/link-make-string \"%:link\" \"%:description\")"
               "%(let ((sel \"%i\"))"
               "   (unless (string-match-p (rx-to-string '(: bos (* space) eos)) sel)"
               "     (format \":\n#+begin_quote\n%s\n#+end_quote\" sel)))\n")
      :immediate-finish t)))

  :config
  (let ((mail-modes (rx (or "message-mode"
                            "gnus-summary-mode"
                            "gnus-article-mode"))))
    (setq org-capture-templates-contexts
          `(("f" ((in-mode . ,mail-modes)))
            ("s" ((in-mode . ,mail-modes)))
            ("p" ((in-mode . "fundamental-mode"))))))

  (define-advice org-capture (:after (&rest _args) fullscreen)
    (when (string-equal (frame-parameter nil 'name) "emacs-popup")
      (delete-other-windows)))

  (define-advice org-capture-finalize (:after (&optional stay-with-capture) delete-frame)
    "Delete frame after `org-capture-finalize' if it is tagged as a temporary popup.
As `org-capture-finalize' is called from `org-capture-refile', in which case we need to defer
deleting the frame until `org-capture-refile' is finished (see advice below)."
    (and (string-equal (frame-parameter nil 'name) "emacs-popup")
         (not (eql this-command 'org-capture-refile))
         (not stay-with-capture)
         (delete-frame)))

  (define-advice org-capture-refile (:after (&rest _args) delete-frame)
    (when (string-equal (frame-parameter nil 'name) "emacs-popup")
      (delete-frame)))

  (defun my:org/host-from-url (url)
    "Return the host part from URL."
    (string-remove-prefix "www." (url-host (url-generic-parse-url url))))

  (defun my:org/link-make-string (link &optional description)
    (let ((host (my:org/host-from-url link)))
      (org-link-make-string
       link
       (save-match-data
         (cond ((equal host "news.ycombinator.com")
                "? on news.ycombinator.com")
               ((or (not description) (string-empty-p description))
                host)
               ;; Mastodon etc.
               ((string-match (rx-let ((author (+ (not ":")))
                                       (cite (: ?\" (+? anything) ?\"))
                                       (page-title (+ anything)))
                                (rx bos (group-n 1 author) ": " cite " - " page-title eos))
                              description)
                (format "%s on %s" (match-string 1 description) host))
               ;; fallback
               ((format "%s: %s" host description)))))))

  (defun my:org/open-buffer-for-capture-to-kill-ring ()
    "Set up a buffer that will lead to the capture going to the kill ring."
    (let* ((name " *org-capture-to-kill-ring*")
           (_ (ignore-errors (kill-buffer name)))
           (buffer (get-buffer-create name)))
      (set-buffer buffer)
      (org-mode)
      ;; Disable storing a bookmark, as the buffer is not visiting a file.
      (setq-local org-capture-bookmark nil)))

  (defun hooks:org/capture-to-kill-ring ()
    "Conditionally store the captured text in the kill ring."
    (when (string-prefix-p " *org-capture-to-kill-ring*"
                           (buffer-name (buffer-base-buffer)))
      ;; TODO: Do these need to be set in `org-capture-current-plist'?
      (org-capture-put :immediate-finish t
                       :no-save t
                       :kill-buffer t)
      (goto-char (point-max))
      (unless (or (= (line-number-at-pos) 1)
                  (eq (char-before) ?\n))
        (ignore-errors (insert "\n")))
      (copy-region-as-kill (point-min) (point-max))))
  (add-hook 'org-capture-prepare-finalize-hook #'hooks:org/capture-to-kill-ring))

(use-package org-clock
  :no-require t
  :after org

  :custom
  (org-clock-into-drawer t)
  (org-clock-rounding-minutes 0)
  (org-clock-history-length 23)
  (org-clock-in-resume t)
  (org-clock-out-when-done t)
  (org-clock-out-remove-zero-time-clocks t)
  (org-clock-report-include-clocking-task t)
  (org-clock-clocked-in-display 'both)
  (org-clock-mode-line-total 'current)
  (org-clock-idle-time nil)
  (org-clock-persist t)

  :config
  (defun org-clock-add-note+ (&optional select)
    "Go to most recently clocked entry and `org-add-note'.
With prefix arg SELECT, offer recently clocked tasks for selection."
    (interactive "@P")
    (save-window-excursion
      (org-clock-goto select)
      (org-add-note))))

(use-package oc
  :no-require t
  :after org

  :general
  (:keymaps 'org-mode-map
            "C-c b" 'org-cite-insert)

  :custom
  (org-cite-global-bibliography '("~/read/read.bib")))

(use-package oc-basic
  :demand t
  :after oc)

(use-package ox-md
  :demand t
  :after ox

  :custom
  (org-md-toplevel-hlevel 2)

  :config
  (define-advice org-md-template (:filter-args (args) inject-title-as-headline)
    ""
    (let* ((contents (car args))
           (info (cadr args))
           (toplevel (plist-get info :md-toplevel-hlevel))
           (title (plist-get info :title)))
      (if (and (eq toplevel 2) title)
          (list (concat
                 (org-md--headline-title (plist-get info :md-headline-style) 1 (org-export-data title info) nil)
                 contents)
                info)
        args))))

(use-package ox-latex
  :demand t
  :after ox

  :custom
  (org-latex-default-class "scrartcl")
  (org-latex-prefer-user-labels t)
  (org-latex-tables-booktabs t)
  (org-latex-remove-logfiles t)
  (org-latex-image-default-width "")
  (org-latex-compiler "pdflatex")
  (org-latex-bib-compiler "biber")
  (org-latex-compiler-file-string "%% -*- LaTeX-command: %s -*-\n")
  (org-latex-pdf-process
   '("latexmk -pdflatex='%latex -file-line-error -halt-on-error' -pdf -outdir=%o %f"))
  (org-preview-latex-default-process 'imagemagick)

  :config
  ;; Use graphicsmagick instead of imagemagick
  (when (executable-find "gm")
    (let ((entry (alist-get 'imagemagick org-preview-latex-process-alist)))
      (setf (cl-getf entry :image-converter)
            '("gm convert -density %D -trim -antialias %f -quality 100 %O"))
      (cl-callf2 remove "convert" (cl-getf entry :programs))
      (cl-pushnew "gm" (cl-getf entry :programs) :test 'equal)))

  ;; TODO! Are these two redundant?
  (setq image-scaling-factor 1.5)
  (setf (plist-get org-format-latex-options :scale) 1.5)

  (add-to-list
   'org-latex-classes
   '("scrartcl"
     "\\documentclass{scrartcl}"
     ("\\section{%s}" . "\\section*{%s}")
     ("\\subsection{%s}" . "\\subsection*{%s}")
     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
     ("\\paragraph{%s}" . "\\paragraph*{%s}")
     ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

  (add-to-list 'org-latex-logfiles-extensions "fls")
  (add-to-list 'org-latex-logfiles-extensions "pyg")
  (add-to-list 'org-latex-logfiles-extensions "fdb_latexmk")

  (add-to-list 'org-latex-packages-alist '("AUTO" "babel" t ("pdflatex")))
  (add-to-list 'org-latex-packages-alist '("AUTO" "polyglossia" t ("xelatex" "lualatex")))

  (add-to-list 'org-latex-packages-alist '("" "booktabs" nil)))

(use-package ox-beamer
  :demand t
  :after ox

  :config
  (add-to-list 'org-beamer-environments-extra
               '("onlyenv" "O" "\\begin{onlyenv}%a" "\\end{onlyenv}"))
  (add-to-list 'org-beamer-environments-extra
               '("visibleenv" "V" "\\begin{visibleenv}%a" "\\end{visibleenv}")))

(use-package ox-koma-letter
  :demand t
  :after ox)

(use-package ob-core
  :no-require t
  :after org

  :config
  (defun org-flash-context+ (&optional duration)
    (interactive)
    (let ((element (org-element-context)))
      (when (fboundp 'nav-flash-show)
        (nav-flash-show
         (org-element-property :begin element)
         (org-element-property :end element)
         'highlight (or duration 0.5)))))

  (define-advice org-babel-execute-src-block (:before (&optional _arg info _params) flash)
    "Flash `org-babel' src blocks when they are executed."
    (save-excursion
      (let ((pos (or org-babel-current-src-block-location
                     (nth 5 info)
                     (org-babel-where-is-src-block-head))))
        (when pos
          (goto-char pos))
        (org-flash-context+ 1.0))))

  (defun org-maybe-redisplay-inline-images+ ()
    "Refresh inline images if they are already shown."
    (interactive)
    (when org-inline-image-overlays
      (org-redisplay-inline-images)))
  (add-hook 'org-babel-after-execute-hook #'org-maybe-redisplay-inline-images+))

(use-package ol-eww
  :demand t
  :after (ol eww))

(use-package ol-gnus
  :demand t
  :after (ol gnus-sum))

(use-package org-id
  :demand t
  :after org)

(use-package org-habit
  :demand t
  :after org

  :custom
  (org-habit-graph-column 48))

(use-package org-expiry
  :demand t
  :after org

  :custom
  (org-expiry-inactive-timestamps t)
  (org-expiry-handler-function 'org-expiry-add-keyword)

  :config
  (defun hooks:org/expiry-insert-created-for-all-todo-entries ()
    "Insert expiry information for all TODO entries in the capture buffer."
    (org-map-entries #'org-expiry-insert-created "/!"))
  (add-hook 'org-capture-prepare-finalize-hook #'hooks:org/expiry-insert-created-for-all-todo-entries)
  (org-expiry-insinuate)

  (define-advice org-expiry-insert-created (:around (fun &rest args) inhibit-based-on-file-tag)
    (unless (or (member "noexpiry" org-file-tags)
                (when (fboundp 'org-roam-file-p)
                  (org-roam-file-p)))
      (apply fun args))))

(use-package org-inlinetask
  :demand t
  :after org

  :custom
  (org-inlinetask-default-state "TODO"))

(use-package org-protocol
  :demand t
  :after org

  :config
  (defun org-protocol-capture-html (info)
    "Process INFO similar to `org-protocol-capture', but convert to Org markup."
    (unless (json-plist-p info)
      (message "Old style org-protocol links aren't supported.")
      (user-error "Old style org-protocol links aren't supported."))
    (let* ((body (or (plist-get info :body) "")))
      (with-temp-buffer
        (insert body)
        (unless (zerop (call-process-region
                        (point-min) (point-max) "pandoc" 'replace t nil
                        "-f" "html" "-t" "org"
                        "--lua-filter" (no-littering-expand-etc-file-name
                                        "org-capture-html-filter.lua")
                        "--wrap=none" "--sandbox"))
          (error "Pandoc failed with non-zero exit status: %s" (buffer-string)))
        (goto-char (point-min))
        (while (re-search-forward "[\u00A0\u2011]" nil t)
          (replace-match (cl-case (preceding-char)
                           (?\u00A0 " ") ; no-break space
                           (?\u2011 "-") ; non-breaking hyphen
                           (t ""))))
        (setq body (buffer-string)))
      (org-protocol-capture (plist-put info :body body))))

  (setf (alist-get "capture-html" org-protocol-protocol-alist nil nil 'equal)
        '(:protocol "capture-html" :function org-protocol-capture-html :kill-client t)))

(defun org-roam-insert-heading+ (&optional arg)
  (interactive "P")
  (org-insert-heading arg)
  (org-id-get-create))

(use-package org-roam
  :diminish ""
  :defer t

  :general
  ( :keymaps 'org-mode-map
    "C-c i" 'org-roam-node-insert
    "C-c C-<return>" 'org-roam-insert-heading+)
  (my:general/leader
    "y a" 'org-roam-alias-add
    "y A" 'org-roam-alias-remove
    "y t" 'org-roam-tag-add
    "y T" 'org-roam-tag-remove
    "y d" '(:keymap org-roam-dailies-map :package org-roam-dailies)
    "y c" 'org-roam-capture
    "y r" 'org-roam-refile
    "y f" 'org-roam-node-find
    "y g" 'org-roam-graph
    "y TAB" 'org-roam-buffer-toggle
    "y y" 'org-roam-buffer-display-dedicated)

  :custom
  (org-roam-database-connector 'sqlite-builtin)
  (org-roam-directory (expand-file-name "~/zettelkasten/"))
  ;; If there are spaces here (e.g., via the `*' field width) they get stored in the completion-read history.
  (org-roam-node-display-template "${title}")
  (org-roam-extract-new-file-path "${slug}.org")
  (org-roam-capture-templates
   '(("d" "default" plain "%?"
      :if-new (file+head "${slug}.org"
                         "#+title: ${title}\n#+language: en")
     :unnarrowed t)))
  (org-roam-capture-ref-templates
   '(("r" "ref" plain "%?"
      :if-new (file+head "${slug}.org"
                         "#+title: ${title}\n#+language: en")
      :unnarrowed t)))
  (org-roam-dailies-capture-templates
    `(("d" "default" entry "* %?"
       :if-new (file+head "%<%Y-%m-%d>.org"
                          "#+title: %<%Y-%m-%d>\n#+language: en"))))
  (org-roam-graph-link-hidden-types '("file" "http" "https"))
  (org-roam-graph-max-title-length 30)
  (org-roam-graph-extra-config
   `(("ranksep" . "\"0.4 equally\"")
     ("newrank" . "true")
     ("concentrate" . "true")
     ("ordering" . "out")
     ("splines" . "ortho")
     ("stylesheet" . ,(json-encode
                       (no-littering-expand-etc-file-name "org-roam.css")))))
  ;; use peripheries with zero penwidth to fake white border around node
  (org-roam-graph-node-extra-config
   '(("id" . (("shape" . "box")
              ("style" . "filled")
              ("fillcolor" . "gray95")
              ("penwidth" . "0")
              ("peripheries" . "2")
              ("fontname" . "IBM Plex Sans")
              ("fontsize" . "11")
              ("height" . "0.25")))))
  (org-roam-graph-edge-extra-config
   '(("arrowsize" . "0.5")
     ("color" . "gray65")))

  :config
  (with-eval-after-load 'org-roam-id
    (when (fboundp 'org-roam-id-open)
      ;; Reset :follow property of `id' links and use advice instead.
      ;; TODO: Remove once this is fixed upstream
      (org-link-set-parameters "id" :follow #'org-id-open)
      (advice-add 'org-id-find :before-until #'org-roam-id-find)))

  (setf (alist-get "\\*org-roam\\*" display-buffer-alist nil nil 'equal)
        '(display-buffer-in-side-window
          (side . right)
          (window-width . 0.45)
          (window-height . fit-window-to-buffer)
          (window-parameters . ((no-other-window . t)
                                (no-delete-other-windows . t)))))

  (add-hook 'org-roam-buffer-postrender-functions #'magit-section-show-level-2)
  (add-hook 'org-roam-mode-hook #'adaptive-wrap-prefix-mode)
  (add-hook 'org-roam-mode-hook #'toggle-word-wrap)
  (add-hook 'org-roam-mode-hook #'toggle-truncate-lines)

  (defun my:org-roam/preview-element-at-point ()
    ""
    (save-excursion
      (cond ((org-in-item-p)              ; prefer enclosing item
             (org-beginning-of-item)
             ;; Move away from the beginning, else the whole list is selected if
             ;; we are at the first item.
             (forward-char))
            ((org-at-table-p)             ; prefer enclosing table
             (goto-char (org-table-begin)))))
    (or
     (when-let* ((element (org-element-at-point-no-context))
                 (beg (org-element-property :begin element))
                 (end (org-element-property :end element)))
       (when-let ((next-element (save-excursion
                                  (goto-char end)
                                  (org-element-at-point-no-context))))
         ;; If a paragraph ends in `:' and the next element is a quote,
         ;; include that.
         (and (eq (org-element-type element) 'paragraph)
              (memq (org-element-type next-element) '(plain-list quote-block))
              (save-excursion
                (goto-char end)
                (skip-chars-backward " \r\t\n" beg)
                (eq (char-before) ?:))
              (setq end (org-element-property :end next-element))))
       (string-trim-right (buffer-substring-no-properties beg end)))
     ""))

  (setq org-roam-preview-function #'my:org-roam/preview-element-at-point)

  (defun org-roam-rename-according-to-title+ ()
    ""
    (interactive)
    (let ((old-name (buffer-file-name)))
      (unless (and (org-roam-buffer-p)
                   old-name
                   (org-roam-file-p old-name))
        (user-error "not a file-visiting org-roam buffer"))
      (org-roam-db-update-file old-name)
      (let* ((slug (org-roam-node-slug (save-excursion
                                         (goto-char (point-min))
                                         (org-roam-node-at-point 'assert))))
             ;; TODO: .org.gpg?
             (new-name (expand-file-name (concat slug ".org"))))
        (cl-assert (org-roam-file-p new-name) 'show-args)
        (unless (file-equal-p old-name new-name)
          (rename-file old-name new-name)
          (set-visited-file-name new-name nil 'along-with-file)))))

  (defvar-local my:org-roam/buffer-update-timer nil)
  (setq org-roam-db-update-on-save nil)

  (defun org-roam-db-update-buffer-when-idle+ (&optional buffer)
    (unless buffer
      (setq buffer (or (buffer-base-buffer) (current-buffer))))
    (when (buffer-live-p buffer)
      (with-current-buffer buffer
        (when my:org-roam/buffer-update-timer
          (cancel-timer my:org-roam/buffer-update-timer))
        (if (and (current-idle-time)
                 (not (active-minibuffer-window)))
            (let ((file (buffer-file-name buffer)))
              (message "Indexing %s" file)
              (org-roam-db-update-file file))
          ;; (re-)schedule for later
          (setq my:org-roam/buffer-update-timer
                (run-with-idle-timer
                 2 nil #'org-roam-db-update-buffer-when-idle+ buffer))))))

  (define-advice org-roam-db-autosync--try-update-on-save-h (:override () defer-update)
    (org-roam-db-update-buffer-when-idle+))

  (when (file-directory-p org-roam-directory)
    (org-roam-db-autosync-mode)))



(defun tmux-refresh-environment+ ()
  "Update environment variables if they have changed in the parent tmux session."
  (interactive)
  (let ((envs (shell-command-to-string "tmux showenv")))
    (save-match-data
      (mapcar
       (lambda (line)
         (when (string-match "^-?\\([^=]+\\)\\(?:=\\(.*\\)\\)?" line)
           (let ((name (match-string 1 line))
                 (value (match-string 2 line)))
             (setenv name value)
             line)))
       (split-string envs "\n")))))

(defun dos2unix+ ()
  (interactive)
  (set-buffer-file-coding-system 'undecided-unix))

(defun unix2dos+ ()
  (interactive)
  (set-buffer-file-coding-system 'undecided-dos))

(defun tabify-buffer+ ()
  "Tabify the whole buffer."
  (interactive)
  (tabify (point-min) (point-max)))

(defun untabify-buffer+ ()
  "Untabify the whole buffer."
  (interactive)
  (untabify (point-min) (point-max)))

(defun indent-buffer+ ()
  "Reindent the whole buffer."
  (interactive)
  (indent-region (point-min) (point-max)))

(defun reverse-paragraphs+ (beg end)
  "Reverse the order of paragraphs in the region."
  (interactive "r")
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (let* ((sort-lists (sort-build-lists
                          (function
                           (lambda ()
                             (while (and (not (eobp)) (looking-at paragraph-separate))
                               (forward-line 1))))
                          'forward-paragraph
                          nil nil))
             (old (reverse sort-lists)))
        (sort-reorder-buffer sort-lists old)))))

(use-package mark-hard-newlines
  :defer t

  :ghook
  ('use-hard-newlines-hook #'my:mark-hard-newlines/toggle-with-use)

  :config
  (defun my:mark-hard-newlines/toggle-with-use ()
    (mark-hard-newlines-mode (if use-hard-newlines 1 -1))))

(defvar default-messages-to-show 4
  "Default number of messages for `show-some-recent-messages'.")

(defun show-some-recent-messages+ (count)
  "Show COUNT last lines of the `*Messages*' buffer."
  (interactive "P")
  (setq count (if count (prefix-numeric-value count)
                default-messages-to-show))
  (let ((message-log-max nil))
    (message
     "%s"
     (with-current-buffer "*Messages*"
       (save-mark-and-excursion
        (goto-char (point-max))
        (buffer-substring-no-properties
         (progn
           (skip-chars-backward "[:space:]")
           (point))
         (progn
           (forward-line (- (1- count)))
           (point))))))))
(my:general/leader "P" 'show-some-recent-messages+)

(defun cycle-tab-width+ ()
  "Cycle `tab-width' through the values 2, 4 and 8."
  (interactive)
  (setq tab-width (mod (* tab-width 2) 14))
  (message "tab-width set to %d" tab-width)
  (set-transient-map
   (let ((map (make-sparse-keymap)))
     (define-key map (vector last-input-event) #'cycle-tab-width+)
     map)))

(defun narrow-or-widen-dwim+ (p)
  "If the buffer is narrowed, it widens.  Otherwise, it narrows intelligently.
Intelligently means: region, org-src-block, org-subtree, or defun,
whichever applies first.
Narrowing to org-src-block actually calls `org-edit-src-code'.

With prefix P, don't widen, just narrow even if buffer is already
narrowed."
  (interactive "P")
  (cond ((and (buffer-narrowed-p) (not p)) (widen))
        ((region-active-p)
         (narrow-to-region (region-beginning) (region-end)))
        ((and (boundp 'org-src-mode) org-src-mode (not p))
         (org-edit-src-exit))
        ((derived-mode-p 'org-mode)
         (cond ((ignore-errors (org-edit-src-code) t)
                (delete-other-windows))
               ((ignore-errors (org-edit-special) t)
                (delete-other-windows))
               ((ignore-errors (org-narrow-to-block) t))
               (t (org-narrow-to-subtree))))
        ((derived-mode-p 'LaTeX-mode)
         (LaTeX-narrow-to-environment))
        ((and (boundp 'ess-noweb-mode) ess-noweb-mode
              (fboundp 'mark-knitr-noweb-chunk)
              (fboundp 'edit-indirect-region)
              (save-excursion
                (when (mark-knitr-noweb-chunk)
                  (edit-indirect-region (region-beginning) (region-end) t)
                  t))))
        ((and (fboundp 'edit-indirect-commit)
              (fboundp 'edit-indirect-buffer-indirect-p)
              (edit-indirect-buffer-indirect-p)
              (not p))
         (edit-indirect-commit))
        (t (narrow-to-defun))))

(general-define-key "C-x C-n" 'narrow-or-widen-dwim+)
(general-define-key :keymaps 'ctl-x-map "n" 'narrow-or-widen-dwim+)
(general-define-key :keymaps 'org-src-mode-map "\C-x\C-s" 'org-edit-src-exit)

(defun hooks:latex/remove-narrow-bindings ()
  "Remove dynamically created default binding for `C-x n'."
  (general-define-key :keymaps 'LaTeX-mode-map "\C-xn" nil))
(add-hook 'LaTeX-mode-hook #'hooks:latex/remove-narrow-bindings)

;; Inspired by https://github.com/alphapapa/unpackaged.el
(defun fill-paragraph-widen-incrementally+
    (&optional unfill fill-function)
  "Fill paragraph increasing the fill column on consecutive invocations.
When the prefix argument UNFILL is non-nil, unfill the paragraph.

FILL-FUNCTION is the function to use for filling at point and defaults
to `fill-paragraph'."
  (interactive "P")
  (let* ((fill-function (or fill-function 'fill-paragraph))
         (prev-value (get this-command 'flex-fill-column))
         (fill-column
          (cond (unfill most-positive-fixnum)
                ((equal last-command this-command)
                 (or (my:next-fill-column prev-value fill-function)
                     fill-column))
                (t fill-column))))
    (put this-command 'flex-fill-column
         (unless unfill fill-column))
    (message "Fill column: %s"
             (if (< fill-column most-positive-fixnum)
                 fill-column
               "unfill"))
    (funcall fill-function)))

(defun org-fill-paragraph-widen-incrementally+
    (&optional unfill)
  "Fill paragraph increasing the fill column on consecutive invocations.
When the prefix argument UNFILL is non-nil, unfill the paragraph."
  (interactive "P")
  (fill-paragraph-widen-incrementally+ unfill 'org-fill-paragraph))

(defun my:next-fill-column (&optional start fill-function)
  "Return next `fill-column' value >= START that would cause filling.

FILL-FUNCTION is the function to use for filling at point and defaults
to `fill-paragraph'."
  (cl-macrolet ((without-changing-buffer
                 (&rest body)
                 `(save-excursion
                    (catch 'revert-changes
                      (atomic-change-group
                        (throw 'revert-changes
                               (progn ,@body)))))))
    (without-changing-buffer
     (cl-loop
      with fill-column = (or start fill-column)
      with original-fill-column = fill-column
      with hash = (buffer-hash)
      while (and (funcall (or fill-function
                              'fill-paragraph))
                 (string= hash (buffer-hash)))
      if (> (- fill-column original-fill-column) 100)
      return nil
      else do (cl-incf fill-column)
      finally return fill-column))))

(general-define-key
 [remap fill-paragraph] #'fill-paragraph-widen-incrementally+
 [remap c-fill-paragraph] #'fill-paragraph-widen-incrementally+)
(general-define-key
 :keymaps 'org-mode-map [remap fill-paragraph]
 #'org-fill-paragraph-widen-incrementally+)

(defun reposition-window-top-bottom+ (&optional arg)
  "Call `reposition-window' with ARG then cycle like `recenter-top-bottom'."
  (interactive "P")
  (if (eql this-command last-command)
      (recenter-top-bottom)
    (reposition-window arg)
    (setq recenter-last-op nil)))
(general-mmap "zf" 'reposition-window-top-bottom+)

(defun bury-or-kill-current-buffer+ (&optional kill)
  "Bury or kill the current buffer.

When the prefix argument KILL is non-nil, kill the buffer instead of burying it."
  (interactive "P")
  (if kill
      (kill-buffer)
    (bury-buffer)))
(my:general/leader
  "d" 'bury-or-kill-current-buffer+
  "D" 'unbury-buffer)



(let ((local-config (no-littering-expand-etc-file-name "local.el")))
  (when (file-exists-p local-config)
    (with-demoted-errors "Could not load local config: %S" (load-file local-config))))

(progn
  (message "Loading %s...done (%.3fs)" user-init-file
           (float-time (time-subtract (current-time)
                                      before-user-init-time)))
  (add-hook
   'after-init-hook
   (lambda ()
     (with-current-buffer "*scratch*"
       (emacs-lock-mode 'kill))

     (message
      "Loading %s...done (%.3fs) [after-init]" user-init-file
      (float-time (time-subtract after-init-time
                                 before-user-init-time))))
   'append))

;;; init.el ends here
